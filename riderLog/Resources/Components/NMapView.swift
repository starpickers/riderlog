//
//  NMapView.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/16.
//

import SwiftUI
import NMapsMap
import UIKit
import CoreLocation

final class NaverMapViewController: UIViewController{
    
    var mapView: NMFNaverMapView = NMFNaverMapView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
    
    var cameraDelegate: NMFMapViewCameraDelegate!
    var optionDelegate: NMFMapViewOptionDelegate!
    var touchDelegate: NMFMapViewTouchDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.showZoomControls = false
        mapView.showLocationButton = false
        mapView.showCompass = false
        mapView.showScaleBar = false
        
        mapView.mapView.isStopGestureEnabled = false
        mapView.mapView.zoomLevel = 16
        mapView.mapView.positionMode = .compass
        
        mapView.mapView.addCameraDelegate(delegate: cameraDelegate)
        mapView.mapView.addOptionDelegate(delegate: optionDelegate)
        mapView.mapView.touchDelegate = touchDelegate
        
        view.addSubview(mapView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //현재 위치로 이동
        mapView.mapView.moveCamera(NMFCameraUpdate(scrollTo: mapView.mapView.locationOverlay.location))
    }
}

struct NaverMap: UIViewControllerRepresentable {
    
    @Binding var positionMode: NMFMyPositionMode
    
    class Coordinator: NSObject, NMFMapViewTouchDelegate, NMFMapViewCameraDelegate, NMFMapViewOptionDelegate{

        var parent: NaverMap
        
        init(_ parent: NaverMap) {
            self.parent = parent
        }

        func mapViewOptionChanged(_ mapView: NMFMapView) {

        }
        
        func mapView(_ mapView: NMFMapView, cameraDidChangeByReason reason: Int, animated: Bool) {
            if reason == 0 {
                //API 호출로 인한 카메라 업데이트
                mapView.positionMode = .compass
            }
            if mapView.positionMode != .compass {
                parent.positionMode = .disabled
            }
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<NaverMap>) -> NaverMapViewController {
        
        let VCNaverMap = NaverMapViewController()
        
        VCNaverMap.cameraDelegate = context.coordinator
        VCNaverMap.optionDelegate = context.coordinator
        VCNaverMap.touchDelegate = context.coordinator
         
        return VCNaverMap
    }
    
    func updateUIViewController(_ uiViewController: NaverMapViewController, context: Context) {
        if positionMode == .compass {
            uiViewController.mapView.mapView.positionMode = .compass
        }
    }
}

struct RouteMapView: UIViewRepresentable {
    var geopoints: [[String: Any]]
    
    func makeUIView(context: Context) -> NMFNaverMapView {
        let mapView = NMFNaverMapView()
        mapView.showZoomControls = false
        mapView.showScaleBar = false
        mapView.showCompass = false
        
        mapView.mapView.isTiltGestureEnabled = false
        
        DispatchQueue.global(qos: .default).async {
            if geopoints.count == 0 { return }
            
            let points = geopoints.map({NMGLatLng(lat: $0["lat"] as! Double, lng: $0["lon"] as! Double)})
            DispatchQueue.main.async {
                mapView.mapView.maxZoomLevel = 17
                mapView.mapView.minZoomLevel = 5

                let south = geopoints.map{$0["lon"] as! Double}.min() ?? 122.37
                let west = geopoints.map{$0["lat"] as! Double}.min() ?? 31.43
                let north = geopoints.map{$0["lon"] as! Double}.max() ?? 132
                let east = geopoints.map{$0["lat"] as! Double}.max() ?? 44.35
                let bounds = NMGLatLngBounds(southWest: NMGLatLng(lat: west - 1, lng: south - 1), northEast: NMGLatLng(lat: east + 1, lng: north + 1))
                mapView.mapView.extent = bounds
                mapView.mapView.moveCamera(NMFCameraUpdate(scrollTo: NMGLatLng(lat: (west + east)/2, lng: (south + north)/2)))
                
                let polyline = NMFPolylineOverlay(points)
                polyline?.color = UIColor(Color(hex: 0xF4811F))
                polyline?.width = 7
                polyline?.capType = .round
                polyline?.joinType = .round

                polyline?.mapView = mapView.mapView
            }
        }
        return mapView
    }

    func updateUIView(_ uiView: NMFNaverMapView, context: Context) {
    }
}
