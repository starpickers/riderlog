//
//  EditableTextField.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/08.
//

import SwiftUI

struct EditableTextField: View {
    @Binding var text: String
    var title: String = "title"
    var placeholder: String? = nil
    
    var isValid: ((String) -> Bool) = { _ in true }
    var errMsg: String? = nil
    
    var body: some View {
        HStack {
            Text(title)
            
            Spacer()
            
            ZStack {
                HStack {
                    TextField(placeholder ?? title, text: $text)
                }
                .foregroundColor(Color(hex: 0x555555))
                .padding(.vertical, 12)
                .padding(.horizontal, 18)
                .background(isValid(text) ? Color(hex: 0xFAFAFA) : Color(hex: 0xFCF4F4))
                .cornerRadius(25)
                
                if !isValid(text) {
                    HStack(spacing: 2) {
                        Image(systemName: "exclamationmark.circle")
                            .font(Font.system(size: 10, weight: .bold))
                        
                        Text(errMsg ?? "ERROR")
                            .font(Font.system(size: 10, weight: .bold))
                        
                        Spacer()
                    }
                    .foregroundColor(.red)
                    .offset(x: 10, y: 32)
                }
            }
            .frame(width: 240)
        }
    }
}

struct EditableTextField_Previews: PreviewProvider {
    static var previews: some View {
        EditableTextField(text: .constant("title"))
    }
}
