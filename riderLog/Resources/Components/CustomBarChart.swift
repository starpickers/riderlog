//
//  CustomBarChart.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/04.
//

import SwiftUI

struct BarChartView: View {
    @State var data: [Double]
    var index: [String]?
    var primaryColor: Color
    var secondaryColor: Color = Color.white
    var grid = 3
    @Binding var selectedIndex: Int
    @State private var touchLocation: CGFloat = -1
    
    func normalizedValue(index: Int) -> Double {
        guard let max = data.max() else {
            return 1
        }
        if max != 0 {
            return Double(data[index])/Double(max)
        } else {
            return 0
        }
    }
    
    func barIsTouched(index: Int) -> Bool {
        touchLocation > CGFloat(index)/CGFloat(data.count) && touchLocation < CGFloat(index+1)/CGFloat(data.count)
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                VStack {
                    ForEach(0..<grid) { i in
                        Spacer()
                        if i == grid - 1 {
                            Divider()
                                .frame(height: 2)
                                .background(Color.gray)
                        } else {
                            DashedLine()
                                .stroke(style: StrokeStyle(lineWidth: 1, dash: [4]))
                                .frame(height: 1)
                                .foregroundColor(Color.gray)
                        }
                    }
                }
                .padding(.top, 30)
                
                HStack(spacing: geometry.size.width/CGFloat(data.count) * 5/16){
                    ForEach(0..<data.count) { i in
                        VStack(spacing: 0){
                            Spacer()
                            
                            if barIsTouched(index: i) || selectedIndex == i {
                                if index != nil {
                                    Text(index![i])
                                        .fixedSize()
                                        .foregroundColor(selectedIndex == i ? primaryColor : .gray)
                                        .frame(height: 20, alignment: .top)
                                        .font(.system(size: 12, weight: .bold))
                                } else {
                                    Text(String(format: "%.1f", data[i]))
                                        .fixedSize()
                                        .foregroundColor(selectedIndex == i ? primaryColor : .gray)
                                        .frame(height: 20, alignment: .top)
                                        .font(.system(size: 12, weight: .bold))
                                }
                            }
                            
                            BarChartCell(value: normalizedValue(index: i), primaryColor: selectedIndex == i ? .blue : .gray, secondaryColor: selectedIndex == i ? secondaryColor : .white, index: data[i], size: geometry.size)
                                .opacity(barIsTouched(index: i) ? 1 : 0.7)
                                .scaleEffect(barIsTouched(index: i) ? CGSize(width: 1.02, height: 1) : CGSize(width: 1, height: 1), anchor: .bottom)
                        }
                        .animation(.spring())
                    }
                }
                .padding(.horizontal, 10)
                .offset(y: -2)
                .gesture(DragGesture()
                    .onChanged({ value in
                        let width = geometry.frame(in: .local).width
                        self.touchLocation = value.location.x/width
                    })
                )
            }
            .onAppear() {
                touchLocation = (CGFloat(selectedIndex) + 0.5)/CGFloat(data.count)
            }
        }
    }
}

struct MarkerShape: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        let cornerRadius: CGFloat = [rect.maxY/5, rect.maxY/5].min()!/3
        let peakRadius: CGFloat = [rect.maxY/5, rect.maxY/5].min()!/8
        let width = rect.maxX
        let height = rect.maxY
        let peakHeight: CGFloat = [rect.maxY/5, rect.maxY/5].min()!

        path.move(to: CGPoint(x: 0, y: 0))
        path.addArc(center: CGPoint(x: cornerRadius, y: cornerRadius),
                    radius: cornerRadius,
                    startAngle: Angle(radians: -Double.pi/2),
                    endAngle: Angle(radians: Double.pi),
                    clockwise: true)
        path.addArc(center: CGPoint(x: cornerRadius, y: height - peakHeight - cornerRadius),
                    radius: cornerRadius,
                    startAngle: Angle(radians: -Double.pi),
                    endAngle: Angle(radians: Double.pi/2),
                    clockwise: true)
        path.addArc(center: CGPoint(x: width * 0.5 - (peakHeight + cornerRadius - peakRadius * (tan(Double.pi/3) - 1))*tan(Double.pi/6), y: height - peakHeight + cornerRadius),
                    radius: cornerRadius,
                    startAngle: Angle(radians: -Double.pi/2),
                    endAngle: Angle(radians: -Double.pi/6),
                    clockwise: false)
        path.addArc(center: CGPoint(x: width * 0.5, y: height - peakRadius), radius: peakRadius,
                    startAngle: Angle(radians: Double.pi/3*2),
                    endAngle: Angle(radians: Double.pi/3),
                    clockwise: true)
        path.addArc(center: CGPoint(x: width * 0.5 + (peakHeight + cornerRadius - peakRadius * (tan(Double.pi/3) - 1))*tan(Double.pi/6), y: height - peakHeight + cornerRadius),
                    radius: cornerRadius,
                    startAngle: Angle(radians: -Double.pi/6*5),
                    endAngle: Angle(radians: -Double.pi/2),
                    clockwise: false)
        path.addArc(center: CGPoint(x: width - cornerRadius, y: height - peakHeight - cornerRadius),
                    radius: cornerRadius,
                    startAngle: Angle(radians: Double.pi/2),
                    endAngle: Angle(radians: 0),
                    clockwise: true)
        path.addArc(center: CGPoint(x: width - cornerRadius, y: cornerRadius),
                    radius: cornerRadius,
                    startAngle: Angle(radians: 0),
                    endAngle: Angle(radians: -Double.pi/2),
                    clockwise: true)
        path.closeSubpath()

        return path
    }
}

struct BarChartCell: View {
    var value: Double
    var primaryColor: Color
    var secondaryColor: Color?
    var index: Any
    var size: CGSize
                         
    var body: some View {
        RoundedRectangle(cornerRadius: 8)
            .fill(LinearGradient(gradient: Gradient(colors: [primaryColor, primaryColor, secondaryColor ?? primaryColor]), startPoint: .top, endPoint: .bottom))
            .frame(height: (size.height - 30) * value)
    }
}
