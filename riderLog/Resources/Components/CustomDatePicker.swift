//
//  CustomDatePicker.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/02.
//

import SwiftUI

struct DateValue: Identifiable {
    var id = UUID().uuidString
    var day: Int
    var date: Date
    var riding: Bool = false
}

extension Date {
    var daysOfMonth : [Date] {
        let calendar = Calendar.current
        let range = calendar.range(of: .day, in: .month, for: self.startOfMonth)!
        return range.compactMap{ calendar.date(byAdding: .day, value: $0 - 1, to: self.startOfMonth)}
    }
    
}

struct CustomDatePicker: View {
    @EnvironmentObject var generalManager: GeneralManager
    @EnvironmentObject var alertData: AlertItemData
    
    @Binding var isShown: Bool
    @Binding var currentDate: Date
    @Binding var records: [Any]?
    @Binding var monthlyData: [[Double]]?
    
    @State var selectDate: Date = Date()
    @State var isDisabled: Bool = false

    func extractDate() -> [DateValue] {
        let date = currentDate
        var days = date.daysOfMonth.compactMap { date -> DateValue in
            return DateValue(day: date.day, date: date)
        }
        
        let firstWeekday = Calendar.current.component(.weekday, from: days.first?.date ?? Date())
        
        for _ in 0..<firstWeekday - 1 {
            days.insert(DateValue(day: -1, date: Date()), at: 0)
        }
                
        return days
    }
    
    var body: some View {
        ZStack{
            let days = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]
            
            Color.black.opacity(0.1)
            
            VStack{
                Spacer()
                VStack{
                    HStack(spacing: 20){
                        Text("\(currentDate.toString(format: "MMMM, yyyy"))")
                            .font(.system(size: 14, weight: .black))
                            .foregroundColor(.blue)
                        
                        Spacer()
                        
                        Button {
                            currentDate = Calendar.current.date(byAdding: .month, value: -1, to: currentDate)!
                        } label: {
                            Text("<")
                                .font(.headline)
                                .foregroundColor(.blue)
                        }
                        .padding(.horizontal, 3)
                        
                        Button {
                            currentDate = Calendar.current.date(byAdding: .month, value: 1, to: currentDate)!
                        } label: {
                            Text(">")
                                .font(.headline)
                                .foregroundColor(.blue)
                        }
                        .padding(.horizontal, 3)
                    }
                    .padding(.horizontal, 8)
                    .padding(.bottom, 6)
                    
                    VStack {
                        HStack(spacing: 0) {
                            ForEach(days, id: \.self) { day in
                                Text(day)
                                    .font(.system(size: 10, weight: .bold))
                                    .frame(maxWidth: .infinity)
                            }
                        }
                        
                        let columns = Array(repeating: GridItem(.flexible(), spacing: 0), count: 7)
                        
                        LazyVGrid(columns: columns, spacing: 0) {
                            ForEach(extractDate()) { value in
                                DateView(value: value)
                                    .background(
                                        Capsule()
                                            .fill(Calendar.current.isDate(value.date, inSameDayAs: selectDate) ? .blue : .white)
                                            .padding(.horizontal, 6)
                                            .padding(.vertical, 2)
                                    )
                                    .onTapGesture {
                                        selectDate = value.date
                                        if selectDate > Date() {
                                            isDisabled = true
                                        } else {
                                            isDisabled = false
                                        }
                                    }
                            }
                        }
                    }
                    .padding(.vertical, 6)

                    HStack {
                        Button {
                            isShown = false
                        } label: {
                            Text("CANCEL")
                                .padding(.vertical, 8)
                                .frame(maxWidth: .infinity)
                                .font(.system(size: 13, weight: .bold))
                                .foregroundColor(.blue)
                                .background(Color.white)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 25)
                                        .stroke(Color.blue, lineWidth: 1)
                                )
                        }
                        .padding(.horizontal, 3)
                        .padding(.top, 2)
                        
                        Button {
                            isShown = false
                            monthlyData = nil
                            currentDate = selectDate
                            generalManager.getDailyRecord(date: selectDate) { res in
                                if let res = res {
                                    records = [String(format: "%.1f", Float(res.distance)/1000), String(format: "%.1f", Float(res.time)/3600), res.bestSpeed, res.bestAltitude, res.bestBank]
                                } else {
                                    records = nil
                                }
                            }
                            generalManager.getMonthlyRecord(date: selectDate) { res in
                                monthlyData = res
                            }
                        } label: {
                            Text("SET")
                                .padding(.vertical, 8)
                                .frame(maxWidth: .infinity)
                                .font(.system(size: 13, weight: .bold))
                                .foregroundColor(.white)
                                .background(Color.blue)
                                .cornerRadius(25)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 25)
                                        .stroke(Color.blue, lineWidth: 1)
                                )
                        }
                        .opacity(isDisabled ? 0.5 : 1.0)
                        .disabled(isDisabled)
                        .padding(.horizontal, 3)
                        .padding(.top, 2)
                    }
                }
                .padding(.horizontal, 20)
                .padding(.vertical, 26)
                .frame(maxWidth: max(UIScreen.main.bounds.width*0.72, 300) )
                .background(Color.white)
                .cornerRadius(28)
                .shadow(color: Color(hex: 0x000000, alpha: 0.3), radius: 15, x: 0, y: 0)
//                .padding(.horizontal, UIScreen.main.bounds.width - 330)
                Spacer()
            }
        }
        .onAppear() {
            selectDate = currentDate
        }
    }
    
    @ViewBuilder
    func DateView(value: DateValue) -> some View {
        VStack {
            if value.day != -1 {
                if value.riding {
                    Text("\(value.day)")
                        .font(.system(size: 12, weight: .bold))
                        .frame(maxWidth: .infinity)
                        .foregroundColor(checkDate(date: value.date))

                    Spacer()

                    Circle()
                        .fill(Calendar.current.isDate(value.date, inSameDayAs: selectDate) ? .white : .blue)
                        .frame(width: 6, height: 6)
                } else {
                    Text("\(value.day)")
                        .font(.system(size: 12, weight: .bold))
                        .frame(maxWidth: .infinity)
                        .foregroundColor(checkDate(date: value.date))

                    Spacer()
                }
            }
        }
        .padding(.vertical, 8)
        .frame(height: 44, alignment: .top)
    }
    
    func checkDate(date: Date) -> Color {
        if Calendar.current.isDate(date, inSameDayAs: selectDate) {
            return .white
        } else if Calendar.current.isDate(date, inSameDayAs: Date()) {
            return .blue
        } else {
            return .black
        }
    }

}
