//
//  PhotoPicker.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/16.
//

import SwiftUI
import PhotosUI

extension UIImage {
    func downSample(size: CGSize, scale: CGFloat = UIScreen.main.scale) -> UIImage {
        let imageSourceOption = [kCGImageSourceShouldCache: false] as CFDictionary
        let data = self.pngData()! as CFData
        let imageSource = CGImageSourceCreateWithData(data, imageSourceOption)!
        let maxPixel = max(size.width, size.height) * scale
        let downSampleOptions = [ kCGImageSourceCreateThumbnailFromImageAlways: true,
                                          kCGImageSourceShouldCacheImmediately: true,
                                    kCGImageSourceCreateThumbnailWithTransform: true,
                                           kCGImageSourceThumbnailMaxPixelSize: maxPixel ] as CFDictionary
        let downSampledImage = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, downSampleOptions)!
        let newImage = UIImage(cgImage: downSampledImage)
        
        return newImage
    }
    
    func croppedImage() -> UIImage {
        let sourceSize = self.size
        
        let sideLength = min(
            sourceSize.width,
            sourceSize.height
        )
        let xOffset = (sourceSize.width - sideLength) / 2.0
        let yOffset = (sourceSize.height - sideLength) / 2.0

        let cropRect = CGRect(
            x: xOffset,
            y: yOffset,
            width: sideLength,
            height: sideLength
        ).integral

        let sourceCGImage = self.cgImage!
        let croppedCGImage = sourceCGImage.cropping(
            to: cropRect
        )!
        let croppedImage = UIImage(
            cgImage: croppedCGImage,
            scale: self.imageRendererFormat.scale,
            orientation: self.imageOrientation
        )
        
        return croppedImage
    }
    
    
    func fixedOrientation() -> UIImage {

        if imageOrientation == .up {
            return self
        }

        var transform: CGAffineTransform = CGAffineTransform.identity

        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2)
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2)
        case .up, .upMirrored:
            break
        }

        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }

        if let cgImage = self.cgImage, let colorSpace = cgImage.colorSpace,
            let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) {
            ctx.concatenate(transform)

            switch imageOrientation {
            case .left, .leftMirrored, .right, .rightMirrored:
                ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
            default:
                ctx.draw(cgImage, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            }
            if let ctxImage: CGImage = ctx.makeImage() {
                return UIImage(cgImage: ctxImage)
            } else {
                return self
            }
        } else {
            return self
        }
    }
}

struct ProfilePhotoPicker: UIViewControllerRepresentable {
    @Binding var selectedImage: UIImage?
    var didFinishPicking: (_ didSelectItems: Bool) -> Void
    typealias UIViewControllerType = PHPickerViewController
 
    func makeUIViewController(context: Context) -> PHPickerViewController {
        var config = PHPickerConfiguration()
        config.filter = .images
        config.selectionLimit = 1
        config.preferredAssetRepresentationMode = .current

        let controller = PHPickerViewController(configuration: config)
        controller.delegate = context.coordinator
        return controller
    }
 
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {
 
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(with: self)
    }
    
    func getPhoto(from itemProvider: NSItemProvider) {
        itemProvider.loadObject(ofClass: UIImage.self) { object, error in
            if let error = error {
                print(error.localizedDescription)
            }
 
            if let image = object as? UIImage {
                selectedImage = image.fixedOrientation().croppedImage().downSample(size: CGSize(width: 120 , height: 120))
            }
        }
    }
    
    class Coordinator: PHPickerViewControllerDelegate {
        var photoPicker: ProfilePhotoPicker
     
        init(with photoPicker: ProfilePhotoPicker) {
            self.photoPicker = photoPicker
        }
        
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            photoPicker.didFinishPicking(!results.isEmpty)
            
            if !results.isEmpty {
                self.photoPicker.getPhoto(from: results[0].itemProvider)
                return
            }
        }
    }
}


struct PhotoPicker: UIViewControllerRepresentable {
    @Binding var pickerResult: [UIImage]
    @Binding var isPresented: Bool
    
    func makeUIViewController(context: Context) -> PHPickerViewController {
        var config = PHPickerConfiguration()
        config.filter = .images
        config.selectionLimit = 9 - pickerResult.count
        config.preferredAssetRepresentationMode = .current

        let controller = PHPickerViewController(configuration: config)
        controller.delegate = context.coordinator
        return controller
    }
 
    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {
 
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(with: self)
    }

    func getPhotos(from results: [PHPickerResult]) {
        DispatchQueue.global(qos: .default).async {
            for image in results {
                image.itemProvider.loadObject(ofClass: UIImage.self) { object, error in
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    
                    if let image = object as? UIImage {
                        DispatchQueue.main.async {
                            pickerResult.append(image.fixedOrientation().downSample(size: CGSize(width: 150, height: 150/image.size.width*image.size.height)))
                        }
                    }
                }
            }
        }
    }
    
    class Coordinator: PHPickerViewControllerDelegate {
        private var photoPicker: PhotoPicker
     
        init(with photoPicker: PhotoPicker) {
            self.photoPicker = photoPicker
        }
        
        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
            if !results.isEmpty {
                self.photoPicker.getPhotos(from: results)
            }
            
            photoPicker.isPresented = false
        }
    }
}
