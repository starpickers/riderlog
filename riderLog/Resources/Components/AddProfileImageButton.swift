//
//  AddProfileImageButton.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/08.
//

import SwiftUI

struct AddProfileImageButton: View {
    @Binding var image: UIImage?
    @State private var showSheet: Bool = false
    var color: Color = .black
    
    var body: some View {
        ZStack {
            if image == nil {
                Circle()
                    .strokeBorder(color, lineWidth: 1)
                    .foregroundColor(color)
                    .frame(width: 124, height: 124, alignment: .center)
                
                Image(systemName: "person")
                    .foregroundColor(color)
                    .font(Font.system(size: 64, weight: .ultraLight))
            } else {
                Image(uiImage: self.image!)
                    .resizable()
                    .frame(width: 124, height: 124)
                    .cornerRadius(50)
                    .clipShape(Circle())
                    .overlay(
                        Circle()
                            .strokeBorder(color, lineWidth: 1)
                    )
            }
            
            Button {
                showSheet = true
            } label: {
                ZStack{
                    Circle()
                        .foregroundColor(color)
                        .frame(width: 42, height: 42, alignment: .center)
                    
                    Image(systemName: image == nil ? "plus" : "pencil")
                        .foregroundColor(.white)
                        .font(Font.system(size: 24, weight: .light))
                }
            }
            .offset(x: 36, y: 48)
        }
        .sheet(isPresented: $showSheet) {
            ProfilePhotoPicker(selectedImage: $image) { didSelectItems in
                showSheet = false
            }
        }
    }
}

struct AddProfileImageButton_Previews: PreviewProvider {
    static var previews: some View {
        AddProfileImageButton(image: .constant(nil))
    }
}
