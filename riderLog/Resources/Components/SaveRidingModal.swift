//
//  SaveRidingModal.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/26.
//

import SwiftUI

struct SaveRidingModal: View {
    @EnvironmentObject var generalManager: GeneralManager
    
    var data: RidingDetail
    @State var text: String = ""

    var body: some View {
        ZStack{
            Color.black.opacity(0.1)
            
            VStack{
                Spacer()
                
                VStack(alignment: .center, spacing: 20){
                    Image(systemName: "exclamationmark.triangle")
                        .font(Font.system(size: 42, weight: .bold))
                        .foregroundColor(Color(hex: 0xEB4D3D))
                    
                    VStack(alignment: .center, spacing: 10) {
                        Text("주행 기록 복구")
                            .font(.system(size: 18, weight: .black))
                                            
                        Text("저장되지 않은 주행 기록이 있습니다.\n주행 소감 입력 후 저장해 주세요.")
                            .font(.system(size: 10, weight: .medium))
                            .multilineTextAlignment(.center)
                            .lineSpacing(6)
                    }
                    
                    VStack(spacing: 10) {
                        HStack {
                            Text("출발지")
                            Spacer()
                            Text(data.startPlace)
                        }
                        HStack {
                            Text("도착지")
                            Spacer()
                            Text(data.finishPlace)
                        }
                        HStack {
                            Text("주행 시작 시간")
                            Spacer()
                            Text(data.startAt!.toString(format: "MM월 dd일 HH:mm"))
                        }
                    }
                    .font(.system(size: 10, weight: .bold))
                    .padding(.horizontal, 10)
                    
                    TextEditor(text: $text)
                        .font(.system(size: 12, weight: .medium))
                        .padding(.top, 6)
                        .padding(.horizontal, 10)
                        .frame(maxWidth: .infinity)
                        .frame(height: 130)
                        .background(Color(hex: 0xEEEEEE))
                        .foregroundColor(.black)
                        .cornerRadius(16)
                    
                    Button{
                        generalManager.saveUnsavedData(data: data, comnt: text)
                        UserDefaults.standard.set(nil, forKey:"unsaved riding")
                        
                    } label: {
                        Text("저장")
                            .padding(.vertical, 9)
                            .frame(maxWidth: .infinity)
                            .font(.system(size: 14, weight: .black))
                            .foregroundColor(.white)
                            .background(Color(hex: 0x000000))
                            .cornerRadius(25)
                            .overlay(
                                RoundedRectangle(cornerRadius: 25)
                                    .stroke(Color(hex: 0x000000), lineWidth: 1)
                            )
                    }
                }
                .padding(.horizontal, 24)
                .padding(.vertical, 26)
                .background(Color.white)
                .cornerRadius(28)
                .shadow(color: Color(hex: 0x000000, alpha: 0.3), radius: 15, x: 0, y: 0)
                .padding(.horizontal, (UIScreen.main.bounds.width - 315))
                
                Spacer()
            }
        }
        .onAppear() {
            UITextView.appearance().backgroundColor = .clear
        }
    }
}
