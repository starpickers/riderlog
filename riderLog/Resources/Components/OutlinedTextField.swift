//
//  OutlinedTextField.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/08.
//

import SwiftUI

struct OutlinedTextField: View {
    @Binding var text: String
    var title: String
    var placeholder: String? = nil
    
    var isValid: ((String) -> Bool) = { _ in true }
    @State var isVisible: Bool? = nil
    var errMsg: String? = nil
    
    enum InputType {
        case password, text
    }
    var inputType: InputType = .text
    
    var body: some View {
        VStack(spacing: 5) {
            ZStack {
                ZStack(alignment: .leading) {
                    switch inputType {
                    case .password:
                        HStack{
                            if isVisible ?? false {
                                TextField(placeholder ?? title.uppercased(), text: $text)
                            } else {
                                SecureField(placeholder ?? title.uppercased(), text: $text)
                            }
                            Button(action: { isVisible!.toggle()}) {
                                Image(systemName: (isVisible ?? false) ? "eye" : "eye.slash")
                                    .font(Font.system(size: 16, weight: .medium))
                            }
                        }
                        .outlined(error: !isValid(text))
                    case .text:
                        TextField(placeholder ?? title.uppercased(), text: $text)
                            .outlined(error: !isValid(text))
                    }
                    
                    Text(title.uppercased())
                        .font(.system(size: 10, weight: .heavy))
                        .padding(.horizontal, 4)
                        .background(Color.white)
                        .offset(x: 16, y: -22)
                }
                .font(Font.system(size: 14, weight: .semibold))

                if !isValid(text) {
                    HStack(spacing: 2) {
                        Image(systemName: "exclamationmark.circle")
                            .font(Font.system(size: 10, weight: .bold))
                        
                        Text(errMsg ?? "ERROR")
                            .font(Font.system(size: 10, weight: .bold))
                        
                        Spacer()
                    }
                    .foregroundColor(.red)
                    .offset(x: 10, y: 32)
                }
            }
        }
    }
}

extension View {
    func outlined(error: Bool) -> some View {
        self
            .padding(.horizontal, 18)
            .frame(width: .infinity, height: 44)
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .stroke(error ? Color.red : Color.black, lineWidth: 1.5)
            )
    }
}

struct OutlinedTextField_Previews: PreviewProvider {
    static var previews: some View {
        OutlinedTextField(text: .constant(""), title: "title")
    }
}
