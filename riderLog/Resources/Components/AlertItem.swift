//
//  AlertItem.swift
//  riderLog
//
//  Created by 박서현 on 2021/09/06.
//

import Foundation
import SwiftUI
import Combine

struct AlertItem: Identifiable {
    var id = UUID()
    var title = Text("")
    var message: Text?
    var dismissButton: Alert.Button?
    var primaryButton: Alert.Button?
    var secondaryButton: Alert.Button?
}

class AlertItemData: ObservableObject {
    @Published var alertItem: AlertItem?
}
