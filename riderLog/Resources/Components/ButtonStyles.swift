//
//  Button.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/23.
//

import SwiftUI

enum CustomButtonStyles {
    case basic
    case outlined
}

struct RegularButton: ButtonStyle {
    var width: CGFloat
    func makeBody(configuration: Self.Configuration) -> some View {
        return configuration.label
            .frame(width: width, height: 44)
            .background(Color.black)
            .foregroundColor(.white)
            .font(.system(size: 14, weight: .bold))
            .cornerRadius(25)
            .shadow(color: Color.gray, radius: 4, x: 0, y: 3)
    }
}

struct CustomButton: ButtonStyle {

    var width: CGFloat
    var height: CGFloat
    var fontWeight: Font.Weight
    var fontSize: CGFloat
    var fontColor: UInt
    var backgroundColor: UInt
    var radius: CGFloat
    var type: CustomButtonStyles = .basic
    
    func makeBody(configuration: Self.Configuration) -> some View {
        
        switch type {
        case .outlined :
            if width == .infinity {
                configuration.label
                    .font(.system(size: fontSize, weight: fontWeight))
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: height)
                    .foregroundColor(configuration.isPressed ? Color(hex: fontColor, alpha: 0.5) : Color(hex: fontColor))
                    .overlay(
                        RoundedRectangle(cornerRadius: radius)
                            .stroke( configuration.isPressed ? Color(hex: backgroundColor, alpha: 0.5) : Color(hex: backgroundColor), lineWidth: 1)
                    )
            } else {
                configuration.label
                    .font(.system(size: fontSize, weight: fontWeight))
                    .frame(width: width, height: height, alignment: .center)
                    .foregroundColor(configuration.isPressed ? Color(hex: fontColor, alpha: 0.5) : Color(hex: fontColor))
                    .overlay(
                        RoundedRectangle(cornerRadius: radius)
                            .stroke( configuration.isPressed ? Color(hex: backgroundColor, alpha: 0.5) : Color(hex: backgroundColor), lineWidth: 1)
                    )
            }

        default :
            if width == .infinity {
                configuration.label
                    .font(.system(size: fontSize, weight: fontWeight))
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: height)
                    .foregroundColor(configuration.isPressed ? Color(hex: fontColor, alpha: 0.2) : Color(hex: fontColor))
                    .background(configuration.isPressed ? Color(hex: backgroundColor, alpha: 0.2) : Color(hex: backgroundColor))
                    .cornerRadius(radius)
            } else {
                configuration.label
                    .font(.system(size: fontSize, weight: fontWeight))
                    .frame(width: width, height: height, alignment: .center)
                    .foregroundColor(configuration.isPressed ? Color(hex: fontColor, alpha: 0.2) : Color(hex: fontColor))
                    .background(configuration.isPressed ? Color(hex: backgroundColor, alpha: 0.2) : Color(hex: backgroundColor))
                    .cornerRadius(radius)
            }
        }
    }
}

