//
//  CustomPicker.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/29.
//

import SwiftUI
import Photos

struct Images: Hashable {
    var image: UIImage
    var selected: Bool
}

struct Card: View {
    var data: Images
    @State private var isSelected: Bool = false
    var body: some View {
        ZStack(alignment: .bottomTrailing){
            Image(uiImage: self.data.image)
                .resizable()
                .frame(width: (UIScreen.main.bounds.width - 64)/3, height: (UIScreen.main.bounds.width - 64)/3)
                .onTapGesture {
                    self.isSelected.toggle()
                }
                .border(Color.white, width: isSelected ? 4 : 0)
            
            if isSelected {
                ZStack{
                    Circle()
                        .frame(width: 18, height: 18, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .foregroundColor(.white)
                        .padding(10)
                    Text("0")
                        .font(.system(size: 10, weight: .black))
                        .foregroundColor(.black)
                }
            }
        }
    }
    
}

struct CustomPicker: View {
    
    @State var selectedImages: [UIImage] = []
    @State var data: [Images] = []
    @State var grid: [[Images]] = []
    
    func getAllImages() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.includeHiddenAssets = false
        
        //최근 9개만 가져오기
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        fetchOptions.fetchLimit = 9
            
        let request = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        DispatchQueue.global(qos: .background).async {
            
            let options = PHImageRequestOptions()
            options.deliveryMode = .highQualityFormat
            options.isSynchronous = true
            
            for i in stride(from: 0, to: request.count, by: 3){
                var data : [Images] = []
                
                for j in i..<i+3 {
                    
                    if j < request.count{
                        
                        PHCachingImageManager.default().requestImage(for: request[j], targetSize: CGSize(width: 120, height: 120), contentMode: .aspectFill, options: options) { (image, _) in
                            
                            let image = Images(image: image!, selected: false)
                            
                            data.append(image)
                        }
                    }
                }
                self.grid.append(data)
            }
        }
    }
    
    
    var body: some View {
        VStack{
            if !self.grid.isEmpty {
                VStack(spacing: 12) {
                    ForEach(self.grid, id: \.self) {i in
                        HStack(spacing: 12) {
                            ForEach(i, id: \.self){j in
                                Card(data: j)
                                    .cornerRadius(4.0)
                            }
                        }
                    }
                }
            }
        }
        .onAppear {
            PHPhotoLibrary.requestAuthorization { (status) in
                
                if status == .authorized {
                    self.getAllImages()
                }else{
                    print("not authorized")
                }
            }
        }
    }
}

struct CustomPicker_Previews: PreviewProvider {
    static var previews: some View {
        CustomPicker()
    }
}
