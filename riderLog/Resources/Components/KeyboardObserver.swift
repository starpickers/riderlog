//
//  KeyboardObserver.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/08.
//

import SwiftUI
import Combine

class KeyboardObserver: ObservableObject {
    private var notificationCenter: NotificationCenter
    @Published private(set) var height: CGFloat = 0
    
    init(center: NotificationCenter = .default) {
        notificationCenter = center
        notificationCenter.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    deinit {
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillShowNotification,object: nil)
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillHideNotification,object: nil)
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo as? [String: Any] else {
            return
        }
        guard let keyboardInfo = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        let keyboardSize = keyboardInfo.cgRectValue.size
        DispatchQueue.main.async {
            self.height = keyboardSize.height
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        DispatchQueue.main.async {
            self.height = 0
        }

    }
}
