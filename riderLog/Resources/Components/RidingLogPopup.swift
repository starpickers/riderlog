//
//  RidingLogDetail.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/09.
//

import SwiftUI

struct RidingLogPopup: View {
    var data: FBRiding
    @State var images: [UIImage?]? = nil
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 26) {
                HStack(alignment: .top) {
                    Text("Started At")
                    
                    Spacer()
                    
                    ZStack(alignment: .trailing)  {
                        Text(data.startPlace)
                        
                        Text(data.startAt.toString(format: "h:mm a"))
                            .font(.system(size: 10, weight: .light))
                            .offset(y: 15)
                        
                    }
                    .foregroundColor(Color(hex: 0x646464))
                }
                
                HStack(alignment: .top) {
                    Text("Finished At")
                    
                    Spacer()
                    
                    ZStack(alignment: .trailing) {
                        Text(data.finishPlace)
                        
                        Text(data.finishAt.toString(format: "h:mm a"))
                            .font(.system(size: 10, weight: .light))
                            .offset(y: 15)
                    }
                    .foregroundColor(Color(hex: 0x646464))
                    
                }
                
                HStack {
                    Text("Time")
                    
                    Spacer()
                    
                    Text("\(String(format: "%.1f", Float(data.totalTime)/3600)) H")
                        .foregroundColor(Color(hex: 0x646464))
                }
                
                HStack {
                    Text("Distance")
                    
                    Spacer()
                    
                    Text("\(String(format: "%.1f", Float(data.totalDistance)/1000)) KM")
                        .foregroundColor(Color(hex: 0x646464))
                }
                
                VStack(alignment: .leading) {
                    Text("Top Record")
                        .font(.system(size: 10, weight: .light))
                        .italic()
                    
                    HStack {
                        VStack{
                            HStack{
                                Image("Icon_Speed")
                                    .resizable()
                                    .frame(width: 16, height: 16)

                                Text("\(data.bestSpeed)")
                                
                                Text("km/h")
                                    .font(.system(size: 10, weight: .light))
                            }
                            .offset(y: 4)
                            
                            Divider()
                                .frame(width: 50)
                            
                            Text("Speed")
                                .font(.system(size: 10, weight: .light))
                                .foregroundColor(.gray)
                        }
                        
                        Spacer()
                        
                        VStack{
                            HStack{
                                Image("Icon_Altitude")
                                    .resizable()
                                    .frame(width: 16, height: 16)

                                Text("\(data.bestAltitude)")
                                
                                Text("m")
                                    .font(.system(size: 10, weight: .light))
                            }
                            .offset(y: 4)
                            
                            Divider()
                                .frame(width: 50)
                            
                            Text("Altitude")
                                .font(.system(size: 10, weight: .light))
                                .foregroundColor(.gray)
                        }
                        
                        Spacer()
                        
                        VStack {
                            HStack{
                                Image("Icon_Banking")
                                    .resizable()
                                    .frame(width: 16, height: 16)

                                Text("\(data.bestBank)")
                                
                                Text("°")
                                    .font(.system(size: 10, weight: .light))
                            }
                            .offset(y: 4)
                            
                            Divider()
                                .frame(width: 50)
                            
                            Text("Banking")
                                .font(.system(size: 10, weight: .light))
                                .foregroundColor(.gray)
                        }
                    }
                }
                .padding(.horizontal, 4)
                
                if images == nil {
                    ProgressView()
                } else {
                    if images!.count != 0 {
                        ScrollView(.horizontal, showsIndicators: false) {
                            HStack {
                                ForEach(images!, id: \.self) { image in
                                    if image != nil {
                                        Image(uiImage: image!.croppedImage())
                                            .resizable()
                                            .frame(width: 100, height: 100)
                                            .cornerRadius(8)
                                    }
                                }
                            }
                        }
                    }
                }
                
                if data.comment != "" {
                    Text("\(data.comment)")
                        .lineSpacing(6)
                        .frame(maxWidth: .infinity, alignment: .topLeading)
                        .padding()
                        .background(Color(hex: 0xF9F9F9))
                        .cornerRadius(8)
                }
            }
        }
        .onAppear() {
            DispatchQueue.global(qos: .userInteractive).async {
                images = data.photoUrls.map{ $0.getImage() }
            }
        }
    }
}

//struct RidingLogDetail_Previews: PreviewProvider {
//    static var previews: some View {
//        RidingLogDetail(data: FBRiding(from: <#Decoder#>))
//    }
//}
