//
//  MapView.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/16.
//

import SwiftUI
import MapKit

import SwiftUI
import MapKit
import CoreLocation

struct MapView: UIViewRepresentable {
    var locationManager = CLLocationManager()
    @Binding var positionMode: MKUserTrackingMode
    
    func setupManager() {
      locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }

    func makeUIView(context: UIViewRepresentableContext<MapView>) -> MKMapView {
        setupManager()
        let mapView = MKMapView(frame: UIScreen.main.bounds)
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow

        return mapView
    }

    func updateUIView(_ view: MKMapView, context: UIViewRepresentableContext<MapView>) {
        positionMode = view.userTrackingMode
        print(positionMode)
    }
}

struct Previews_MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(positionMode: .constant(.follow))
    }
}
