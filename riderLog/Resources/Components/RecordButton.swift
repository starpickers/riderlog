//
//  RecordButton.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/09.
//

import SwiftUI

struct RecordButton: View {
    @Binding var showActionSheet: Bool
    
    var recordState: RiderLog.RecordState
    var sensor: Sensor
    var recordStart: () -> Void

    var body: some View {
        Button(action: {
            if recordState == .STOP {
                recordStart()
            } else {
                self.showActionSheet = true
            }
        }) {
            ZStack{
                Circle()
                    .strokeBorder(LinearGradient(gradient: Gradient(colors: [.white, .gray]), startPoint: .top, endPoint: .bottom), lineWidth: 6)
                    .frame(width: 150, height: 150, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .shadow(color: Color(hex: 0x000000, alpha: 0.8), radius: 10, x: 0, y: 10)
                
                VStack(spacing: 10){
                    
                    if recordState == .STOP {
                        Image("MainPage_Logo")
                            .resizable()
                            .frame(width: 82, height: 47)
                            .padding(.bottom, 5)
                        
                    } else {
                        HStack(alignment: .bottom) {
                            Text("\(Int(max(0, sensor.data.GPSDATA[2])))")
                                .frame(width: 63, alignment: .trailing)
                                .font(.system(size: 48, weight: .black))
                                .foregroundColor(.white)
                            
                            Text("km/h")
                                .font(.system(size: 12, weight: .black))
                                .foregroundColor(.white)
                                .padding(.bottom, 10)
                        }
                        .frame(height: 47)
                        .padding(.bottom, 5)
                    }

                    Ellipse()
                        .frame(width: 100, height: 5, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .foregroundColor(.white)
                                                
                    Text( recordState == .STOP ? "START" : "STOP" )
                        .font(.system(size: 20, weight: .bold))
                        .foregroundColor(.white)
                }
            }
        }
        .disabled( sensor.isConnected ? false : true )
    }
}

struct RecordButton_Previews: PreviewProvider {
    static var previews: some View {
        RecordButton(showActionSheet: .constant(false), recordState: .STOP, sensor: Sensor(isConnected: false, data: Sensor.Data()), recordStart: {})
    }
}
