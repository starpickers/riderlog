//
//  EcallAlert.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/04.
//

import SwiftUI

struct EcallAlert: View {
    @EnvironmentObject var generalManager: GeneralManager
    @State private var timeRemaining: Int = 10
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        ZStack{
            Color.black.opacity(0.1)
            
            VStack{
                Spacer()
                VStack(alignment: .center, spacing: 12){
                    Text("사고 발생")
                        .font(.system(size: 18, weight: .black))
                    
                    Image("EcallPage_Siren")
                        .resizable()
                        .frame(width: 72 * 0.7, height: 60 * 0.8, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .padding(.vertical, 8)
                    
                    Text("\(timeRemaining)")
                        .font(.system(size: 48, weight: .black))
                    
                    Text("카운트다운 종료시\n긴급구호 요청이 발송됩니다.")
                        .font(.system(size: 14, weight: .bold))
                        .multilineTextAlignment(.center)
                        .padding(.bottom, 18)
                        .lineSpacing(6)
                    
                    HStack{
                        Button{
                            generalManager.emergencyCall = .request
                        } label: {
                            Text("즉시 신고")
                                .padding(.vertical, 9)
                                .frame(maxWidth: .infinity)
                                .font(.system(size: 14, weight: .bold))
                                .foregroundColor(.white)
                                .background(Color(hex: 0xEB4D3D))
                                .cornerRadius(25)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 25)
                                        .stroke(Color(hex: 0xEB4D3D), lineWidth: 1)
                                )
                        }
                        
                        Button{
                            generalManager.emergencyCall = .clear
                        } label: {
                            Text("신고 취소")
                                .padding(.vertical, 9)
                                .frame(maxWidth: .infinity)
                                .font(.system(size: 14, weight: .bold))
                                .foregroundColor(Color(hex: 0xEB4D3D))
                                .background(Color.white)
                                .cornerRadius(25)
                                .overlay(
                                    RoundedRectangle(cornerRadius: 25)
                                        .stroke(Color(hex: 0xEB4D3D), lineWidth: 1)
                                )
                        }
                    }
                }
                .padding(.horizontal, 24)
                .padding(.vertical, 26)
                .background(Color.white)
                .cornerRadius(28)
                .shadow(color: Color(hex: 0x000000, alpha: 0.3), radius: 15, x: 0, y: 0)
                .padding(.horizontal, (UIScreen.main.bounds.width - 315))
                
                Spacer()
            }
        }
        .onReceive(timer) { time in
            if self.timeRemaining > 0 {
                self.timeRemaining -= 1
            }
        }
        .onChange(of: timeRemaining) { time in
            if time == 0 {
                let delayTime = DispatchTime.now() + 1.0
                DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                    generalManager.emergencyCall = .request
                })
            }
        }
    }
}

struct EcallAlert_Previews: PreviewProvider {
    static var previews: some View {
        EcallAlert()
    }
}
