//
//  Graphic.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/27.
//

import SwiftUI

struct TopShape: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        let safeArea = UIApplication.shared.windows.first?.safeAreaInsets.top
        let width = rect.maxX
        let height = rect.maxY

        path.move(to: CGPoint(x: rect.minX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: safeArea!))
        path.addCurve(to: CGPoint(x: (width - 150)/2, y: height ), control1: CGPoint(x: 80, y: height*0.6), control2: CGPoint(x: 60, y: height))
        path.addLine(to: CGPoint(x: rect.maxX - (width - 150)/2, y: height))
        path.addCurve(to: CGPoint(x: rect.maxX, y: safeArea!), control1: CGPoint(x: width - 60, y: height), control2: CGPoint(x: width - 80, y: height*0.6))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.minY))
        path.closeSubpath()
        return path
    }
}

struct BottomShape: Shape {

    func path(in rect: CGRect) -> Path {
        var path = Path()

        let height: CGFloat = 40
        let length: CGFloat = 225

        let startX: CGFloat = (rect.maxX - 225)/2
        let midPoint: CGFloat = rect.maxX / 2
        let apex1: CGFloat = (startX + midPoint) / 2
        let apex2: CGFloat = rect.maxX - (startX + midPoint) / 2

        path.move(to: CGPoint(x: rect.minX, y: height))
        path.addLine(to: CGPoint(x: (rect.maxX - 225)/2, y: height))

        path.addCurve(to: CGPoint(x: midPoint, y: 0), control1: CGPoint(x: apex1, y:
            height), control2: CGPoint(x: apex1, y: 0))

        path.addCurve(to: CGPoint(x: midPoint + length/2, y: height), control1: CGPoint(x: apex2, y: 0), control2: CGPoint(x: apex2, y: height))

        path.addLine(to: CGPoint(x: rect.maxX, y: height))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.closeSubpath()

        return path
    }
}
