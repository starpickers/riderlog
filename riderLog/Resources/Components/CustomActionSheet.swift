//
//  CustomActionSheet.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/08.
//

import SwiftUI
import Combine

struct CustomActionSheet: View {
    @Binding var isShowing: Bool
    
    var primaryBtnText: String
    var primaryBtnFunc: () -> Void
    
    var secondaryBtnText: String
    var secondaryBtnFunc: () -> Void
    
    @State var offset = UIScreen.main.bounds.height

    func hide() {
        offset = UIScreen.main.bounds.height
        isShowing = false
    }
    
    var interactiveGesture: some Gesture {
        DragGesture()
            .onChanged({ (value) in
                if value.translation.height > 0 {
                    offset = value.location.y
                }
            })
            .onEnded({ (value) in
                let diff = abs(offset-value.location.y)
                if diff > 40 {
                    hide()
                }
                else {
                    offset = 0
                }
            })
    }
    
    var sheetView: some View {
        VStack{
            Spacer()
            
            VStack(spacing: 30) {
                Image("MainPage_Swipe")
                    .resizable()
                    .frame(width: 72, height: 24, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                HStack{
                    Button(primaryBtnText) {
                        primaryBtnFunc()
                        hide()
                    }
                    .buttonStyle(CustomButton(width: 160, height: 53, fontWeight: .bold, fontSize: 16, fontColor: 0xffffff, backgroundColor: 0x000000, radius: 100))
                    
                    Spacer()
                    
                    Button(secondaryBtnText) {
                        secondaryBtnFunc()
                        hide()
                    }
                    .buttonStyle(CustomButton(width: 160, height: 53, fontWeight: .bold, fontSize: 16, fontColor: 0xffffff, backgroundColor: 0x000000, radius: 100))
                }
                .padding(.horizontal, 17)
                
                Button(action: { hide() }) {
                    Text("뒤로가기")
                        .font(.system(size: 14, weight: .semibold))
                        .foregroundColor(.black)
                }
            }
            .padding(.bottom, 46)
            .frame(width: UIScreen.main.bounds.width, height: 230)
            .background(Color.white)
            .cornerRadius(15)
            .offset(y: offset)
            .gesture(interactiveGesture)
            .onTapGesture {
                hide()
            }
        }
    }
    
    var bodyContet: some View {
        ZStack {
            Rectangle()
                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                .background(Color.black)
                .opacity(0.6)
                .ignoresSafeArea()
            
            sheetView
        }
    }
    
    public var body: some View {
        Group {
            if isShowing {
                bodyContet
            }
        }
        .animation(.default)
        .onReceive(Just(isShowing), perform: { isShowing in
            offset = isShowing ? 0 : UIScreen.main.bounds.height
        })
    }
}
