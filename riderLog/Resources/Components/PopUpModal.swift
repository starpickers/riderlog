//
//  PopUpModal.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/15.
//

import SwiftUI

struct PopUpModal: View {
    @Binding var showScanList: Bool
    
    var body: some View {
        ZStack(alignment: .top){
            //Background
            Color.black.opacity(0)
                .edgesIgnoringSafeArea(.all)
            
            //Modal
            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 12){
                Image("MainPage_Bluetooth")
                    .resizable()
                    .frame(width: 40, height: 40)
                
                Text("센서가 연결되지 않았습니다.")
                
                Button("연결하기") {
                    self.showScanList.toggle()
                }
                .padding(.horizontal, 18)
                .frame(height: 32)
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .stroke(Color.white, lineWidth: 1.5)
                )
                .padding(.vertical, 8)
            }
            .foregroundColor(Color.white)
            .padding(.vertical, 16)
            .frame(maxWidth: 260)
            .font(.system(size: 12, weight: .semibold))
            .background(Color(hex: 0x000000, alpha: 0.7))
            .cornerRadius(16)
            .padding(.top, UIScreen.main.bounds.height*0.33)
        }
    }
}

struct PopUpModal_Previews: PreviewProvider {
    @State static var value = true
    
    static var previews: some View {
        PopUpModal(showScanList: $value)
    }
}
