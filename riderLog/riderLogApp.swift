//
//  riderLogApp.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/08.
//

import SwiftUI
import Firebase

@main
struct riderLogApp: App {
    @UIApplicationDelegateAdaptor var delegate: AppDelegate
    
    @StateObject var alertData = AlertItemData()
    @StateObject var generalManager = GeneralManager()
    @StateObject var authViewModel = AuthViewModel()

    var body: some Scene {
        WindowGroup {
            if authViewModel.user.state == .signIn || isSignedIn {
                NavigateView()
                    .environmentObject(alertData)
                    .environmentObject(generalManager)
                    .environmentObject(authViewModel)
                    .alert(item: $alertData.alertItem) { alertItem in
                        guard let primaryButton = alertItem.primaryButton, let secondaryButton = alertItem.secondaryButton else{
                            return Alert(title: alertItem.title, message: alertItem.message, dismissButton: alertItem.dismissButton)
                        }
                        return Alert(title: alertItem.title, message: alertItem.message, primaryButton: primaryButton, secondaryButton: secondaryButton)
                    }
                    .onAppear() {
                        generalManager.addObserver()
                        generalManager.blemanager.startService()
                        if generalManager.locationmanager.authorizationStatus == .denied {
                            self.alertData.alertItem = AlertItem(title: Text("위치권한 설정이 '안 함'으로 되어있습니다."), message: Text("앱 설정 화면으로 가시겠습니까? \n '아니오'를 선택하시면 앱이 종료됩니다."), primaryButton: .default(Text("예"), action: {
                                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                            }), secondaryButton: .destructive(Text("아니오"), action: {exit(0)}))
                        }
                        
                        if authViewModel.user.detail == Credentials.Detail() {
                            authViewModel.getUserInfo(completionHandler: { isSuccess in
                                if isSuccess {
                                    generalManager.riderLog.userID = authViewModel.user.uid
                                }
                            })
                        } else {
                            generalManager.riderLog.userID = authViewModel.user.uid
                        }
                        
                        UIApplication.shared.statusBarStyle = .lightContent
                        UIApplication.shared.setStatusBarStyle(.lightContent, animated: true)
                    }
            } else {
                ZStack{
                    InitialView()
                        .environmentObject(alertData)
                        .environmentObject(generalManager)
                        .environmentObject(authViewModel)
                        .alert(item: $alertData.alertItem) { alertItem in
                            guard let primaryButton = alertItem.primaryButton, let secondaryButton = alertItem.secondaryButton else{
                                return Alert(title: alertItem.title, message: alertItem.message, dismissButton: alertItem.dismissButton)
                            }
                            return Alert(title: alertItem.title, message: alertItem.message, primaryButton: primaryButton, secondaryButton: secondaryButton)
                        }
                        .onAppear() {
                            UIApplication.shared.statusBarStyle = .darkContent
                            UIApplication.shared.setStatusBarStyle(.darkContent, animated: true)
                        }
                    
                    if authViewModel.isLoading {
                        VStack(spacing: 18) {
                            Text("로딩 중...")
                                .font(.system(size: 14, weight: .bold))
                            ProgressView()
                                .progressViewStyle(CircularProgressViewStyle())
                                .scaleEffect(x: 1.4, y: 1.4, anchor: .center)
                        }
                        .frame(width: 140, height: 100, alignment: .center)
                        .background(Color.white)
                        .cornerRadius(16)
                        .shadow(color: Color(hex: 0x000000, alpha: 0.3), radius: 12, x: 0, y: 0)
                        .padding(.bottom, UIScreen.main.bounds.height/12)
                        .allowsHitTesting(true)
                    }
                }
            }
        }
    }
}


