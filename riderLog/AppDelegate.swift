//
//  AppDelegate.swift
//  riderLog
//
//  Created by 박서현 on 2021/08/20.
//

import Foundation
import Firebase
import FirebaseAuth
import FirebaseFirestore
import UIKit
import IQKeyboardManager

public var isSignedIn: Bool = false
public var restoredData: Bool = false

class AppDelegate: NSObject, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        UNUserNotificationCenter.current().delegate = self
        FirebaseApp.configure()
        
        if Auth.auth().currentUser != nil {
            isSignedIn = true
        }
        
        if let _ = UserDefaults.standard.value(forKey:"unsaved riding") {
            restoredData = true
        }
        
        let controlOptions = FirebaseOptions(googleAppID: "1:916033365237:ios:a2f41e61234f42405051bc",
                                               gcmSenderID: "916033365237")
        controlOptions.apiKey = "AIzaSyAxTmwdYWAb8e3u-LI2EnXICgAsOhnIFJw"
        controlOptions.projectID = "riderlog-management-system"
        controlOptions.clientID = "916033365237-qi3epmkhkfg07rtfug3b2nilti4qutv1.apps.googleusercontent.com"
        controlOptions.databaseURL = nil
        controlOptions.storageBucket = "riderlog-management-system.appspot.com"
        
        // Configure an alternative FIRApp.
        FirebaseApp.configure(name: "__FIRAPP_CONTROL", options: controlOptions)
        
//        IQKeyboardManager.shared().isEnabled = true
//        IQKeyboardManager.shared().isEnableAutoToolbar = false
//        IQKeyboardManager.shared().shouldShowToolbarPlaceholder = false
//        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
//        IQKeyboardManager.shared().previousNextDisplayMode = .alwaysHide
//        IQKeyboardManager.shared().keyboardDistanceFromTextField = 50

        sleep(2)
        
        return true
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(.banner)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
}
