//
//  FindPasswordView.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/15.
//

import SwiftUI

struct ResetPasswordView: View {
    @EnvironmentObject var authViewModel: AuthViewModel
    
    @State private var email: String = ""
    @State private var error: Bool? = nil
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            Image("Logo")
                .resizable()
                .frame(width: 144, height: 46)
                .padding(.bottom, UIScreen.main.bounds.height/17)

            VStack(spacing: 60) {
                VStack(spacing: 18) {
                    Text("비밀번호 재설정")
                        .font(.system(size: 20, weight: .black))

                    Text("이메일 주소를 입력하시면, 비밀번호 재설정\n링크를 보내드리겠습니다.")
                        .multilineTextAlignment(.center)
                        .lineSpacing(6)
                }

                OutlinedTextField(text: $email, title: "E-mail")
                
                ZStack {
                    Button("재설정") {
                        self.error = nil
                        authViewModel.resetPassword(email: email, completionHandler: { isSuccess in
                            self.error = !isSuccess
                        })
                    }
                    .buttonStyle(RegularButton(width: 200))
                    
                    if error != nil {
                        HStack(spacing: 2) {
                            Image(systemName: "exclamationmark.circle")
                            if error! {
                                Text("전송 실패! 이메일 주소를 확인해주세요.")
                            } else {
                                Text("전송 성공! 이메일을 확인해주세요.")
                            }
                        }
                        .font(Font.system(size: 12, weight: .bold))
                        .foregroundColor(error! ? .red : .green)
                        .offset(y: 44)
                    }
                }
            }

            Spacer()
            
            HStack(spacing: 5) {
                Text("또는 다음으로 돌아가기")
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Text("로그인")
                        .font(.system(size: 12, weight: .bold))
                }
            }
            .padding(.bottom, 15)
        }
        .padding(.horizontal, 36)
        .foregroundColor(.black)
        .font(Font.system(size: 12, weight: .semibold))
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button {
            self.presentationMode.wrappedValue.dismiss()
        } label: { Image(systemName: "arrow.backward")
                    .foregroundColor(.black)
                    .font(Font.system(size: 16, weight: .medium))
        })
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct ResetPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            ResetPasswordView()
        }
    }
}
