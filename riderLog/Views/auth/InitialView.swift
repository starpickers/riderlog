//
//  InitialView.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/08.
//

import SwiftUI
import UIKit
import Combine

struct InitialView: View {
    @EnvironmentObject var generalManager: GeneralManager
    @EnvironmentObject var authViewModel: AuthViewModel
    
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var isVisible: Bool = false
    @State private var isSuccess: Bool = false
    
    @State private var error: Bool = false

    var body: some View {
        NavigationView {
            VStack(alignment: .center, spacing: 0) {
                NavigationLink(destination: FillUserInfoView(), tag: .throughSNS, selection: $authViewModel.selectionView) { EmptyView() }
                NavigationLink(destination: SignUpWithEmail(), tag: .throughEmail, selection: $authViewModel.selectionView) { EmptyView() }
                
                Spacer()
                
                Image("Logo")
                    .resizable()
                    .frame(width: 144, height: 46)
                    .padding(.bottom, UIScreen.main.bounds.height/10)
                
                VStack(spacing: 72) {
                    VStack(spacing: 40){
                        VStack(spacing: 16) {
                            VStack(spacing: 28) {
                                OutlinedTextField(text: $email, title: "e-mail")

                                OutlinedTextField(text: $password, title: "PASSWORD", placeholder: "비밀번호", isVisible: isVisible, inputType: .password)
                            }
                            
                            HStack{
                                Spacer()
                                NavigationLink(destination: ResetPasswordView(), label: {
                                    Text("비밀번호를 잊으셨나요?")
                                })
                            }
                            .padding(.horizontal, 10)
                        }

                        
                        ZStack {
                            Button("로그인") {
                                self.error = false

                                authViewModel.signIn(email: email, password: password, completionHandler: { isSuccess, error in
                                    if let _ = error {
                                        self.error = true
                                    }
                                    
                                    if isSuccess && authViewModel.user.state == .signUp {
                                        authViewModel.selectionView = .throughEmail
                                    }
                                })
                            }
                            .buttonStyle(RegularButton(width: 200))
                            
                            if error {
                                HStack(spacing: 2) {
                                    Image(systemName: "exclamationmark.triangle")
                                    Text("로그인 실패")
                                }
                                .font(Font.system(size: 12, weight: .bold))
                                .foregroundColor(.red)
                                .offset(y: 44)
                            }
                        }
                    }

                    VStack(spacing: 28) {
                        Text("OR")
                        
                        HStack(spacing: 24) {
//                                Button{
//
//                                } label: {
//                                    Image("InitialPage_KakaoLogin")
//                                        .resizable()
//                                        .aspectRatio(contentMode: .fit)
//                                        .frame(width: 44)
//                                        .shadow(color: .gray, radius: 1.5, x: 0, y: 2)
//                                }
//
//                                Button{
//
//                                } label: {
//                                    Image("InitialPage_NaverLogin")
//                                        .resizable()
//                                        .aspectRatio(contentMode: .fit)
//                                        .frame(width: 44)
//                                        .shadow(color: .gray, radius: 1.5, x: 0, y: 2)
//                                }
                            
                            Button{
                                authViewModel.signInWithGoogle(completionHandler: { isSuccess, error in
                                    if let _ = error {
                                        self.error = true
                                    }
                                })
                            } label: {
                                Image("InitialPage_GoogleLogin")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 44)
                                    .shadow(color: .gray, radius: 1.5, x: 0, y: 2)
                            }

                            SignUpWithAppleView(authViewModel: authViewModel)
                                .frame(width: 44)
                                .shadow(color: .gray, radius: 1.5, x: 0, y: 2)

                        }
                        .frame(height: 44)
                    }
                }

                Spacer()
                
                HStack{
                    Text("회원이 아니신가요?")
                    
                    Button("회원가입 하기") {
                        authViewModel.selectionView = .throughEmail
                    }
                    .font(.system(size: 12, weight: .heavy))
                }
                .padding(.bottom, 15)
            }
            .padding(.horizontal, 36)
            .navigationBarHidden(true)
        }
        .foregroundColor(.black)
        .font(.system(size: 12, weight: .semibold))
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct InitialView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            InitialView()
                .environmentObject(AuthViewModel())
        }
    }
}
