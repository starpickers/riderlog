//
//  FillUserInfoView.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/12.
//

import SwiftUI
import Combine

struct FillUserInfoView: View {
    @EnvironmentObject var authViewModel: AuthViewModel
    
    @State private var email: String = ""
    @State private var name: String = ""
    @State private var phone: String = ""
    @State private var day: String = ""
    @State private var month: String = ""
    @State private var year: String = ""
    
    @State private var isActive : Bool = false
    @Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            NavigationLink(destination: CreateNicknameView(), isActive: $isActive) { EmptyView() }
                
            Image("Logo")
                .resizable()
                .frame(width: 144, height: 46)
                .padding(.bottom, UIScreen.main.bounds.height/17)
            
            VStack(spacing: 48) {
                VStack(spacing: 18) {
                    Text("기본 정보")
                        .font(.system(size: 20, weight: .black))
                    
                    Text("E-call 서비스 이용을 위하여\n연락 가능한 전화번호를 입력해 주세요.")
                        .multilineTextAlignment(.center)
                        .lineSpacing(6)
                }
                
                VStack(spacing: 28) {
                    if authViewModel.user.detail.email == ""  {
                        OutlinedTextField(text: $email, title: "e-mail", isValid: { email in
                            authViewModel.isValidEmail(email)
                        } , errMsg: "올바른 이메일 주소를 입력 해 주세요.")
                    }
                    
                    OutlinedTextField(text: $name, title: "name", placeholder: "이름")
                    
                    OutlinedTextField(text: $phone, title: "phone", placeholder: "전화번호", isValid: { phone in
                        authViewModel.isValidPhoneNumber(phone)
                    } , errMsg: "올바른 전화 번호를 입력 해 주세요.")
                        .keyboardType(.numberPad)
                        .onReceive(Just(phone)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                phone = filtered
                            }
                        }

                    ZStack {
                        ZStack(alignment: .leading) {
                            HStack(spacing: 18) {
                                TextField("YYYY", text: $year)
                                    .outlined(error: authViewModel.isValidDate("\(year)-\(month)-\(day)") == nil && ( year != "" || month != "" || day != "") )
                                
                                TextField("MM", text: $month)
                                    .outlined(error: authViewModel.isValidDate("\(year)-\(month)-\(day)") == nil && ( year != "" || month != "" || day != "") )
                                
                                TextField("DD", text: $day)
                                    .outlined(error: authViewModel.isValidDate("\(year)-\(month)-\(day)") == nil && ( year != "" || month != "" || day != "") )
                            }
                            .keyboardType(.numberPad)

                            Text("BIRTH")
                                .font(.system(size: 10, weight: .heavy))
                                .padding(.horizontal, 4)
                                .background(Color.white)
                                .offset(x: 16, y: -22)
                        }
                        
                        if authViewModel.isValidDate("\(year)-\(month)-\(day)") == nil && ( year != "" || month != "" || day != "") {
                            HStack(spacing: 2) {
                                Image(systemName: "exclamationmark.circle")
                                    .font(Font.system(size: 10, weight: .bold))
                                
                                Text("올바른 날짜를 입력해 주세요.")
                                    .font(Font.system(size: 10, weight: .bold))
                                
                                Spacer()
                            }
                            .foregroundColor(.red)
                            .offset(x: 10, y: 32)
                        }
                    }
                }
                
                Button("다음") {
                    authViewModel.saveUserInfo(name: name, birth: authViewModel.isValidDate("\(year)-\(month)-\(day)"), phone: phone)
                    isActive = true
                }
                .buttonStyle(RegularButton(width: 200))
                .disabled( ( authViewModel.user.detail.email != "" || authViewModel.isValidEmail(email)) && (name != "") && (phone != "")  && authViewModel.isValidDate("\(year)-\(month)-\(day)") != nil ? false : true)
                .opacity( ( authViewModel.user.detail.email != "" || authViewModel.isValidEmail(email)) && (name != "") && (phone != "") && authViewModel.isValidDate("\(year)-\(month)-\(day)") != nil ? 1 : 0.5)
            }

            Spacer()
            
            HStack(spacing: 5) {
                Text("이미 회원이신가요?")
                
                Button("로그인") {
                    authViewModel.selectionView = nil
                    authViewModel.deleteAccount()
                }
                .font(.system(size: 12, weight: .heavy))
            }
            .padding(.bottom, 15)
        }
        .foregroundColor(.black)
        .font(.system(size: 12, weight: .semibold))
        .padding(.horizontal, 36)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button {
            self.presentationMode.wrappedValue.dismiss()
            authViewModel.deleteAccount()
        } label: { Image(systemName: "arrow.backward")
                    .foregroundColor(.black)
                    .font(Font.system(size: 16, weight: .medium))
        })
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct FillUserInfoView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            FillUserInfoView()
                .environmentObject(AuthViewModel())
        }
    }
}
