//
//  SignInWithAppleController.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/23.
//

import SwiftUI
import AuthenticationServices
import CryptoKit
import Firebase


struct SignUpWithAppleView: UIViewRepresentable {
    @ObservedObject var authViewModel: AuthViewModel

    func makeCoordinator() -> SignInWithAppleCoordinator {
        return SignInWithAppleCoordinator(self, authViewModel)
    }
    
    func makeUIView(context: Context) -> UIButton {
         let button = UIButton()
         let image = UIImage(named: "InitialPage_AppleLogin")
         button.setImage(image , for: .normal)
         button.addTarget(context.coordinator, action: #selector(SignInWithAppleCoordinator.didTapButton),for: .touchUpInside)
         button.contentMode = .scaleAspectFit
         button.frame = CGRect(x: 0, y: 0, width: 44, height: 44)

         return button
     }
    
     func updateUIView(_ uiView: UIButton, context: Context) {

     }
}

class SignInWithAppleCoordinator: NSObject, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    var parent: SignUpWithAppleView?
    var authViewModel: AuthViewModel

    fileprivate var currentNonce: String?
    
    init(_ parent: SignUpWithAppleView, _ authViewModel: AuthViewModel) {
        self.parent = parent
        self.authViewModel = authViewModel

        super.init()
    }
    
    @objc func didTapButton() {
        authViewModel.isLoading = true
        
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
       
        authorizationController.presentationContextProvider = self
        authorizationController.delegate = self
        authorizationController.performRequests()
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        let viewController = UIApplication.shared.windows.last?.rootViewController
        return (viewController?.view.window!)!
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            guard let nonce = currentNonce else {
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            let credential = OAuthProvider.credential(withProviderID: "apple.com", idToken: idTokenString, rawNonce: nonce)
            
            authViewModel.createUser(with: credential, completionHandler: { isSuccess, error in
                
            })
            break
            
        default:
            break
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        self.authViewModel.isLoading = false
        self.authViewModel.user.state = .signOut
        print("!Sign in with Apple erroreAd: \(error)")
    }
    
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> = Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map{ _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if length == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        return result
    }
    
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
}
