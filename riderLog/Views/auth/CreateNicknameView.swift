//
//  CreateNicknameView.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/15.
//

import SwiftUI

struct CreateNicknameView: View {
    @EnvironmentObject var authViewModel: AuthViewModel
    @EnvironmentObject var generalManager: GeneralManager

    @State private var image: UIImage? = nil
    @State private var nickname: String = ""

    @Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            Image("Logo")
                .resizable()
                .frame(width: 144, height: 46)
                .padding(.bottom, UIScreen.main.bounds.height/17)
            
            VStack(spacing: 48) {
                VStack(spacing: 18) {
                    Text("프로필 만들기")
                        .font(.system(size: 20, weight: .black))
                    
//                    Text("닉네임 조건 어쩌구 저쩌구")
//                        .lineSpacing(6)
                }

                VStack(spacing: 50) {
                    AddProfileImageButton(image: $image)

                    OutlinedTextField(text: $nickname, title: "닉네임", isValid: { _ in
                        true
                    }, errMsg: "닉네임 조건에 맞추어 주세요.")
                    
                    Button("완료") {
                        authViewModel.isLoading = true
                        if self.image != nil {
                            generalManager.uploadImage(image: self.image!, folder: "profiles", name: authViewModel.user.uid.uppercased() + "-PROFILE-IMAGE", completionHandler: { url in
                                authViewModel.saveUserInfo(nickname: nickname, photoUrl: url.absoluteString)
                                authViewModel.setUser(merge: false, completionHandler: { isSuccess in
                                    authViewModel.isLoading = true
                                    if isSuccess {
                                        authViewModel.user.state = .signIn
                                    }
                                })
                            })
                        } else {
                            authViewModel.saveUserInfo(nickname: nickname)
                            authViewModel.setUser(merge: false, completionHandler: { isSuccess in
                                authViewModel.isLoading = true
                                if isSuccess {
                                    authViewModel.user.state = .signIn
                                }
                            })
                        }
                    }
                    .buttonStyle(RegularButton(width: 200))
                    .disabled(nickname == "" ? true : false)
                    .opacity(nickname == "" ? 0.5 : 1)
                }
            }

            Spacer()
            
            HStack(spacing: 5) {
                Text("이미 회원이신가요?")
            
                Button("로그인") {
                    authViewModel.selectionView = nil
                    authViewModel.deleteAccount()
                }
                .font(.system(size: 12, weight: .heavy))
            }
            .padding(.bottom, 15)
        }
        .foregroundColor(.black)
        .font(Font.system(size: 12, weight: .semibold))
        .padding(.horizontal, 36)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button{
            self.presentationMode.wrappedValue.dismiss()
            authViewModel.deleteAccount()
        } label: { Image(systemName: "arrow.backward")
                    .foregroundColor(.black)
                    .font(Font.system(size: 16, weight: .medium))
        })
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct CreateNicknameView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            CreateNicknameView()
                .environmentObject(AuthViewModel())
        }
    }
}
