//
//  SignUpWithEmail.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/22.
//

import SwiftUI
import Combine

struct SignUpWithEmail: View {
    @EnvironmentObject var authViewModel: AuthViewModel
    @EnvironmentObject var alertData: AlertItemData
    
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    @State private var isVisible1: Bool = false
    @State private var isVisible2: Bool = false

    @State private var isActive : Bool = false
    @Environment(\.presentationMode) private var presentationMode: Binding<PresentationMode>

    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            NavigationLink(destination: FillUserInfoView(), isActive: $isActive) { EmptyView() }
            
            Image("Logo")
                .resizable()
                .frame(width: 144, height: 46)
                .padding(.bottom, UIScreen.main.bounds.height/17)
            
            VStack(spacing: 48) {
                VStack(spacing: 18) {
                    Text("회원 가입")
                        .font(.system(size: 20, weight: .black))
                    
                    Text("메일 주소는 비밀번호 재설정 시 사용됩니다.\n올바른 메일 주소를 입력해주세요.")
                        .multilineTextAlignment(.center)
                        .lineSpacing(6)
                }
                
                VStack(spacing: 60) {
                    VStack(spacing: 28) {
                        OutlinedTextField(text: $email, title: "e-mail", isValid: { email in
                            authViewModel.isValidEmail(email)
                        }, errMsg: "올바른 이메일 주소를 입력 해 주세요.")
                        
                        OutlinedTextField(text: $password, title: "PASSWORD", placeholder: "비밀번호", isValid: { password in
                            authViewModel.isValidPassword(password)
                        }, isVisible: isVisible1, errMsg: "비밀번호는 8~20자로 이루어져야 합니다.", inputType: .password)
                        
                        OutlinedTextField(text: $confirmPassword, title: "CONFIRM PASSWORD", placeholder: "비밀번호 확인", isValid: { password in
                            authViewModel.isValidPassword(password)
                        }, isVisible: isVisible2, errMsg: "비밀번호가 일치하지 않습니다.", inputType: .password)
                    }
                    
                    Button("회원가입") {
                        authViewModel.createUser(email: email, password: password, completionHandler: { isSuccess, error in
                            if let error = error {
                                self.alertData.alertItem = AlertItem(title: Text("회원가입 실패"), dismissButton: .cancel(Text("확인")))
                            }
                            
                            if isSuccess {
                                isActive = true
                            }
                        })
                    }
                    .buttonStyle(RegularButton(width: 200))
                    .opacity(authViewModel.isValidPassword(password) && confirmPassword != "" && password == confirmPassword && authViewModel.isValidEmail(email) ? 1 : 0.5)
                    .disabled(authViewModel.isValidPassword(password) && confirmPassword != "" && password == confirmPassword && authViewModel.isValidEmail(email) ? false : true)
                }
            }
            
            Spacer()
            
            HStack(spacing: 5) {
                Text("이미 회원이신가요?")
                
                Button("로그인") {
                    self.presentationMode.wrappedValue.dismiss()
                }
                .font(.system(size: 12, weight: .heavy))
            }
            .padding(.bottom, 15)
        }
        .foregroundColor(.black)
        .font(.system(size: 12, weight: .semibold))
        .padding(.horizontal, 36)
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button{
            self.presentationMode.wrappedValue.dismiss()
        } label: { Image(systemName: "arrow.backward")
                    .foregroundColor(.black)
                    .font(Font.system(size: 16, weight: .medium))
        })
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct SignUpWithEmail_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            SignUpWithEmail()
                .environmentObject(AuthViewModel())
        }
    }
}
