//
//  ResultView.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/28.
//

import SwiftUI
import CoreLocation

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
}

struct ResultView: View {
    @EnvironmentObject var generalManager: GeneralManager
    @EnvironmentObject var alertData: AlertItemData
    @ObservedObject private var keyboard: KeyboardObserver = KeyboardObserver()
    
    @State var images: [UIImage] = []
    @State private var showSheet: Bool = false
    @State var text : String = ""
    @State var points: [[String: Any]] = []
    @State var mapImage: UIImage? = nil
    
    @Binding var isPresented: Bool
    @Binding var showTabBar: Bool
    
    @State var loadingMap: Bool = true
    @State var tryingSave: Bool = false
    
    var body: some View {
        ZStack{
            Color.black.opacity(0.85)
            
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 32){
                    VStack(spacing: 56) {
                        VStack(spacing: 40) {
                            VStack(spacing: 12) {
                                Text("오늘 주행은 어떠셨나요?")
                                    .font(.system(size: 14, weight: .bold))
                                
                                Divider()
                                    .frame(width: 192)
                                    .background(Color.white)
                                
                                Text("\(generalManager.riderLog.ridingDetail.startPlace) - \(generalManager.riderLog.ridingDetail.finishPlace)")
                                    .font(.system(size: 10, weight: .bold))
                            }
                            
                            VStack {
                                if loadingMap {
                                    Text("Loading...")
                                    ProgressView()
                                        .progressViewStyle(CircularProgressViewStyle(tint: Color.white))
                                } else {
                                    RouteMapView(geopoints: points)
                                }
                            }
                            .frame(maxWidth: .infinity)
                            .frame(height: 220)
                            .background(Color(hex: 0xffffff, alpha: 0.08))
                            .cornerRadius(16)

                            HStack{
                                TotalRecordCell(title: "총주행거리", icon: "Icon_Distance", data: generalManager.riderLog.ridingDetail.totalDistance/1000, unit: "km")
                                
                                Spacer()
                                Divider()
                                    .frame(width: 1, height: 70, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                    .background(Color(hex: 0xffffff, alpha: 0.5))
                                    .padding(.top, 35)
                                Spacer()
                                
                                TotalRecordCell(title: "총주행시간", icon: "Icon_Time", data: Float(generalManager.timer.time/3600), unit: "hours")
                            }
                            .padding(.horizontal, 8)
                            
                            HStack{
                                BestRecordCell(title: "최대고도", icon: "Icon_Altitude", data: generalManager.riderLog.ridingDetail.bestAltitude, unit: "m")
                                
                                Spacer()
                                
                                BestRecordCell(title: "최고속도", icon: "Icon_Speed", data: generalManager.riderLog.ridingDetail.bestSpeed, unit: "km/h")

                                Spacer()
                                
                                BestRecordCell(title: "최대뱅킹각", icon: "Icon_Banking", data: generalManager.riderLog.ridingDetail.bestBank, unit: "°")
                            }
                            .padding(.horizontal, 12)
                        }
                        
                        VStack(spacing: 40){
                            VStack(spacing: 12) {
                                Text("이번 주행 기록하기")
                                    .font(.system(size: 14, weight: .bold))
                                
                                Divider()
                                    .frame(width: 224)
                                    .background(Color.white)
                                
                                Text("이번 라이딩에 대한 소감을 남겨 주세요.")
                                    .font(.system(size: 10, weight: .bold))
                            }
                            
                            VStack(spacing: 42) {
                                if images.count == 0 {
                                    Button{
                                        showSheet = true
                                    } label: {
                                        VStack(spacing: 16) {
                                            Image(systemName: "plus.circle.fill")
                                                .font(.system(size: 48, weight: .semibold))
                                                .opacity(0.5)
                                            
                                            Text("선택된 사진이 없습니다.")
                                                .font(.system(size: 10, weight: .bold))
                                        }
                                    }
                                } else {
                                    VStack(spacing: 18) {
                                        LazyVGrid(columns: [GridItem(.fixed((UIScreen.main.bounds.width - 80)/3)), GridItem(.fixed((UIScreen.main.bounds.width - 80)/3)), GridItem(.fixed((UIScreen.main.bounds.width - 80)/3))]) {
                                            ForEach(images, id: \.self) { i in
                                                ZStack{
                                                    Image(uiImage: i.croppedImage())
                                                        .resizable()
                                                        .frame(width: (UIScreen.main.bounds.width - 90)/3, height: (UIScreen.main.bounds.width - 90)/3, alignment: .center)
                                                        .clipShape(RoundedRectangle(cornerRadius: 8))
                                                        .padding(.bottom, 3)
                                                    
                                                    Button{
                                                        images = images.filter{ $0 != i }
                                                    } label: {
                                                        Image(systemName: "xmark.circle.fill")
                                                            .font(.system(size: 18, weight: .semibold))
                                                    }
                                                    .offset(x: (UIScreen.main.bounds.width - 90)/6, y: (90 - UIScreen.main.bounds.width)/6)
                                                }
                                            }
                                        }

                                        if images.count < 9 {
                                            Button{
                                                showSheet = true
                                            } label: {
                                                HStack {
                                                    Text("추가하러 가기")
                                                    
                                                    Image(systemName: "plus.circle.fill")
                                                        .font(.system(size: 16, weight: .semibold))
                                                        .opacity(0.8)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            .sheet(isPresented: $showSheet) {
                                PhotoPicker(pickerResult: $images, isPresented: $showSheet)
                            }
                            
                            VStack{
                                TextEditor(text: $text)
                                    .foregroundColor(.white)
                                    .padding(.top, 15)
                                    .padding(.horizontal, 15)
                                
                                Spacer()
                                
                                Divider()
                                    .frame(width: UIScreen.main.bounds.width - 80)
                                    .background(Color.white)
                                    .padding(.bottom, 15)
                            }
                            .frame(maxWidth: .infinity)
                            .frame(height: 130, alignment: .center)
                            .background(Color(hex: 0xffffff, alpha: 0.1))
                            .cornerRadius(8)
                        }
                    }

                    HStack{
                        Button {
                            let group = DispatchGroup()
                            tryingSave = true
                            DispatchQueue.global(qos: .userInteractive).async {
                                var urls: [String] = []
                                if images.count == 0 {
                                    generalManager.saveREC(comnt: text)
                                    DispatchQueue.main.async {
                                        isPresented = false
                                    }
                                    return
                                }
                                for (index, image) in images.enumerated() {
                                    group.enter()
                                    generalManager.uploadImage(image: image, folder: "ridings/photos", name: generalManager.riderLog.ridingDetail.ridingId.uppercased() + "-RIDING-PHOTOS-\(index)") { url in
                                        urls.append(url.absoluteString)
                                        group.leave()
                                    }
                                    group.notify(queue: .main) {
                                        generalManager.saveREC(comnt: text, photos: urls)
                                        DispatchQueue.main.async {
                                            isPresented = false
                                        }
                                    }
                                }
                            }
                        } label: {
                            if tryingSave {
                                ProgressView()
                                    .frame(width: 32, height: 32, alignment: .center)
                            } else {
                                Text("저장 & 종료")
                            }
                        }
                        .buttonStyle(CustomButton(width: UIScreen.main.bounds.width*0.5, height: 39, fontWeight: .bold, fontSize: 14, fontColor: 0x000000, backgroundColor: 0xffffff, radius: 100))
                        
                        Button(action: {
                            self.alertData.alertItem = AlertItem(title: Text("서비스 준비 중 입니다"), dismissButton: .cancel(Text("확인")))
                        }) {
                            ZStack(alignment: .trailing){
                                Text("공유하기")
                                    .frame(width: UIScreen.main.bounds.width*0.34, height: 39, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                    .foregroundColor(.black)
                                    .font(.system(size: 14, weight: .bold))
                                    .background(Color.white)
                                    .cornerRadius(100)
                                    .padding(.trailing, 21)

                                Image("MainPage_Share")
                                    .frame(width: 32, height: 32, alignment: .center)
                                    .background(Color(hex:0x27AE60))
                                    .cornerRadius(24)
                            }
                        }
                    }
                    .disabled(tryingSave)
                }
                .padding(.horizontal, 16)
                .padding(.bottom, (UIApplication.shared.windows.first?.safeAreaInsets.bottom)! + 20)
                .padding(.top, (UIApplication.shared.windows.first?.safeAreaInsets.top)! + 20)
            }
            .animation(.easeOut(duration: 0.16))
        }
        .font(.system(size: 12, weight: .semibold))
        .foregroundColor(.white)
        .ignoresSafeArea(.container)
        .onAppear() {
            generalManager.getRealtimeDB() { response in
                points = response
                loadingMap = false
            }
            UITextView.appearance().backgroundColor = .clear
         }
    }
}

struct TotalRecordCell: View {
    var title: String
    var icon: String
    var data: Float
    var unit: String
    
    var body: some View {
        VStack(spacing: 6) {
            HStack(spacing: 6) {
                Image(icon)
                    .resizable()
                    .frame(width: 16, height: 16, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .foregroundColor(Color.white)
                
                Text(title)
                    .font(.system(size: 12, weight: .bold))
                    .foregroundColor(.white)
            }
            
            VStack(spacing: 6) {
                Text("\(String(format: "%.1f", data))")
                    .font(.system(size: 24, weight: .black))
                    .foregroundColor(.black)
                
                Divider()
                    .frame(width: 80, height: 1)
                    .background(Color(hex: 0x000000, alpha: 0.5))
                
                Text(unit)
                    .font(.system(size: 12, weight: .black))
                    .foregroundColor(Color(hex: 0x000000, alpha: 0.5))
                
            }
            .frame(width: 128, height: 80, alignment: .center)
            .background(Color.white)
            .cornerRadius(8)
            .padding(.top, 10)
        }
    }
}

struct BestRecordCell: View {
    var title: String
    var icon: String
    var data: Int
    var unit: String
    
    var body: some View {
        VStack{
            HStack{
                Text("\(data)")
                    .font(.system(size: 16, weight: .black))
                    .foregroundColor(Color.white)
                
                Text(unit)
                    .font(.system(size: 12, weight: .bold))
                    .foregroundColor(Color(hex: 0xffffff, alpha: 0.5))
            }
            
            Divider()
                .frame(width: 80, height: 1)
                .background(Color(hex: 0xffffff, alpha: 0.5))
            
            HStack{
                Image(icon)
                    .resizable()
                    .frame(width: 16, height: 16)
                    .foregroundColor(Color(hex: 0xffffff, alpha: 0.5))
                
                Text(title)
                    .font(.system(size: 10, weight: .black))
                    .foregroundColor(Color(hex: 0xffffff, alpha: 0.5))
            }
        }
    }
}

struct ResultView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            ResultView(isPresented: .constant(true), showTabBar: .constant(true))
                .environmentObject(GeneralManager())
                .environmentObject(AlertItemData())
                .navigationBarHidden(true)
        }
    }
}




