//
//  MainView.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/08.
//

import SwiftUI
import CoreLocation
import Network
import NMapsMap

struct MainView: View {
    @EnvironmentObject var generalManager: GeneralManager
    
    @Binding var showActionSheet: Bool
    @Binding var showScanList: Bool
    @Binding var showTabBar : Bool
    @Binding var showResult: Bool
    
    @State var positionMode: NMFMyPositionMode = .compass

    var body: some View {
        ZStack(alignment: .top) {
            NaverMap(positionMode: $positionMode)
                .edgesIgnoringSafeArea(.all)
                .blur( radius: generalManager.sensor.isConnected ? 0 : 10 )
                .disabled(generalManager.sensor.isConnected ? false : true)
            
            if !showScanList && !showResult {
                VStack(spacing: 0) {
                    TopShape()
                        .fill(Color(hex: 0x000000, alpha: 0.9))
                        .frame(width: UIScreen.main.bounds.width, height: 42 + (UIApplication.shared.windows.first?.safeAreaInsets.top)!)
                    
                    if generalManager.sensor.isConnected && positionMode != .compass {
                        Button(action: {
                           self.positionMode = .compass
                        }){
                            HStack(spacing: 4) {
                                Image(systemName: "scope")
                                
                                Text("RE-CENTER")
                            }
                            .font(.system(size: 10, weight: .bold))
                            .foregroundColor(.white)
                        }
                        .padding(.horizontal)
                        .frame(height: 28, alignment: .center)
                        .background(Color(hex:0x000000, alpha:0.5))
                        .cornerRadius(24)
                        .padding(.top, 9)
                    }
                    
                    Spacer()
                    
                    BottomShape()
                        .fill(Color(hex: 0x000000, alpha: 0.85))
                        .frame(width: UIScreen.main.bounds.width, height: showTabBar ? 222 : 222 + (UIApplication.shared.windows.first?.safeAreaInsets.bottom)!)
                    
                }
                
                VStack {
                    Spacer()
                    BottomMenu(showActionSheet: $showActionSheet, showTabBar: $showTabBar)
                }
                
                if !generalManager.sensor.isConnected {
                    PopUpModal(showScanList: $showScanList)
                } 
            }
            
            if showScanList {
                ScanListView(showScanList: $showScanList)
            }
            
            if showResult {
                ResultView(isPresented: $showResult, showTabBar: $showTabBar)
            } else {
                LogoBlock(showScanList: $showScanList)
                    .padding(.top, UIApplication.shared.windows.first?.safeAreaInsets.top)
            }
        }
        .ignoresSafeArea(.container)
    }
}

struct LogoBlock: View {
    @EnvironmentObject var generalManager: GeneralManager
    @EnvironmentObject var alertData: AlertItemData
    @Binding var showScanList : Bool
    
    @State var presentAlert: Bool = false
    
    var body: some View {
        HStack(alignment: .center, spacing: 8) {
            Circle()
                .fill(generalManager.sensor.isConnected ? Color.green : Color(hex: 0xBDBDBD))
                .overlay(Circle()
                            .stroke(Color.white, lineWidth: 1.5))
                .frame(width: 10, height: 10)
            
            Spacer()
                .frame(width: 102, height: 33)
        
            Button(action: {
                if !showScanList && generalManager.sensor.isConnected {
                    if generalManager.riderLog.recordState == .START { generalManager.pauseREC() }
                    self.alertData.alertItem = AlertItem(title: Text("이미 연결된 센서가 있습니다."), message: generalManager.riderLog.recordState != .STOP ? Text("새로운 기기와 연결하시겠습니까?\n기존 기기와의 연결은 해지되며 진행중인 레코딩을 종료합니다.") : Text("새로운 기기와 연결하시겠습니까?\n기존 기기와의 연결은 해지됩니다."), primaryButton: .default(Text("OK"), action: {
                        generalManager.disconnect()
                        if generalManager.riderLog.recordState != .STOP {
                            DispatchQueue.global(qos: .default).async {
                                generalManager.stopREC()
                                generalManager.saveREC(comnt: "")
                            }
                        }
                        self.showScanList.toggle()
                    }), secondaryButton: .destructive(Text("Cancel"), action: {
                        if generalManager.riderLog.recordState != .STOP {
                            generalManager.startREC()
                        }
                    }))
                } else {
                    self.showScanList.toggle()
                }
            }, label: {
                Image("MainPage_Arrow")
                    .resizable()
                    .frame(width: 24, height: 12, alignment: .center)
                    .rotationEffect(Angle(degrees: showScanList ? 180 : 0))
            })
        }
    }
}

struct BottomMenu: View {
    @EnvironmentObject var generalManager: GeneralManager
    
    @Binding var showActionSheet: Bool
    @Binding var showTabBar: Bool

    var body: some View {
        ZStack(alignment: .center) {
            HStack{
                VStack(alignment: .leading, spacing: 24){
                    VStack(alignment: .leading, spacing: 6) {
                        Text("Distance")
                        
                        HStack{
                            Text("\(String(format: "%.1f", generalManager.riderLog.ridingDetail.totalDistance/1000))")
                                .font(.system(size: 16, weight: .bold))
                                .foregroundColor(.white)

                            Text("km")
                        }
                    }

                    VStack(alignment: .leading, spacing: 6) {
                        Text("Time")

                        Text(generalManager.timer.time.toTimeFormat())
                            .font(.system(size: 16, weight: .bold))
                            .foregroundColor(.white)
                    }
                }

                Spacer()

                VStack(alignment: .trailing, spacing: 24){
                    VStack(alignment: .trailing, spacing: 6) {
                        Text("Lean Angle")

                        HStack{
                            Text("\(generalManager.riderLog.recordState != .STOP ? String(format: "%.1f", generalManager.sensor.data.ATTDATA[0]) : String(0.0))")
                                .font(.system(size: 16, weight: .bold))
                                .foregroundColor(.white)

                            Text("°")
                        }
                    }

                    VStack(alignment: .trailing, spacing: 6) {
                        Text("Altitude")
                        
                        HStack{
                            Text("\(generalManager.riderLog.recordState != .STOP ? String(format: "%.1f", generalManager.sensor.data.GPSDATA[3]) : String(0.0))")
                                .font(.system(size: 16, weight: .bold))
                                .foregroundColor(.white)
                            
                            Text("m")
                        }
                    }
                }
            }
            .font(.system(size: 12, weight: .medium))
            .foregroundColor(.gray)
            .padding(.horizontal, 16)

            VStack(spacing: 16){
                
                RecordButton(showActionSheet: $showActionSheet, recordState: generalManager.riderLog.recordState, sensor: generalManager.sensor, recordStart: {generalManager.startREC()})

                Button(action: {self.showTabBar.toggle()}, label: {
                    Image("MainPage_Arrow")
                        .resizable()
                        .frame(width: 48, height: 24, alignment: .center)
                        .rotationEffect(Angle(degrees: showTabBar ? 0 : 180))
                })
            }
            .padding(.bottom, showTabBar ? 15 : 15 + (UIApplication.shared.windows.first?.safeAreaInsets.bottom)!)
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            MainView(showActionSheet: .constant(false), showScanList: .constant(false), showTabBar: .constant(false), showResult: .constant(false))
                .environmentObject(GeneralManager())
                .navigationBarHidden(true)
        }
    }
}
