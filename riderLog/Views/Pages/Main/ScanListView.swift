//
//  ScanListView.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/09.
//

import SwiftUI

struct ScanListView: View {
    @Binding var showScanList: Bool
    @State var selected: Int = -1
    
    var body: some View {
        ZStack {
            /// Background
            Color(hex: 0x000000, alpha: 0.8)
            
            /// Contents
            VStack(alignment: .center) {
                Image("MainPage_Bluetooth")
                    .resizable()
                    .frame(width: 40, height: 40)
                    .padding(.bottom, 15)
                
                ScanList(showScanList: $showScanList, selected: $selected)
                    .frame(width: UIScreen.main.bounds.width - 120)
                
                Button(action: {showScanList = false}) {
                    Image(systemName: "xmark.circle")
                        .font(.system(size: 28, weight: .light))
                }
            }
            .foregroundColor(.white)
            .font(.system(size: 12, weight: .semibold))
        }
    }
}

struct ScanList: View{
    @EnvironmentObject var generalManager: GeneralManager
    
    @Binding var showScanList: Bool
    @Binding var selected: Int
    
    var body: some View {
        VStack{
            HStack{
                Text("센서 시리얼 번호")
                    .frame(width: 125, alignment: .center)
                
                Spacer()
                
                Text("신호강도")
                    .frame(width: 60, alignment: .center)
            }
            .foregroundColor(Color.customGray)
            .padding(.horizontal, 15)
            
            Divider()
                .background(Color.customGray)
            
            ScrollView(.vertical) {
                VStack(spacing: 0){
                    ForEach(generalManager.blemanager.peripherals){ peripheral in
                        Button(action: {
                            self.selected = peripheral.id
                        }) {
                            HStack {
                                Text(peripheral.name)
                                    .frame(width: 125, alignment: .center)
                                    .foregroundColor(Color.white)
                                
                                Spacer()
                                
                                Text(String(peripheral.rssi))
                                    .frame(width: 60, alignment: .center)
                                    .foregroundColor(Color(hex: 0x00ffff))
                            }
                            .frame(height: 40)
                            .background( self.selected == peripheral.id ? Color(hex: 0xffffff, alpha: 0.2) : Color.clear)
                        }
                    }
                }
            }
            .frame(height: UIScreen.main.bounds.height*0.4, alignment: .top)
            .padding(.horizontal, 15)
            
            Divider()
                .background(Color.customGray)
            
            Text("연결할 센서를 선택해 주세요.")
                .foregroundColor(Color.customGray)
                .padding(.top, 10)
            
            Button("확인") {
                self.generalManager.connect(to: generalManager.blemanager.peripherals[selected].peripheral)
                self.showScanList.toggle()
            }
            .padding(.horizontal, 18)
            .frame(height: 36)
            .overlay(
                RoundedRectangle(cornerRadius: 8)
                    .stroke(Color.white, lineWidth: 1.5)
            )
            .disabled(selected > -1 ? false : true)
            .opacity(selected > -1 ? 1 : 0.5)
            .padding(.vertical, 25)
        }
        .onAppear() {
            generalManager.startScan()
        }
    }
}

struct ScanListView_Previews: PreviewProvider {
    static var previews: some View {
        ScanListView(showScanList: .constant(true))
            .environmentObject(GeneralManager())
    }
}
