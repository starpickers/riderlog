//
//  LogView.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/16.
//

import SwiftUI

struct LogView: View {
    @EnvironmentObject var generalManager: GeneralManager
    
    var graph = ["Dictance", "Time", "Speed", "Altitude", "Lean Angle"]
    var graphIcon = ["Icon_Distance", "Icon_Time", "Icon_Speed", "Icon_Altitude", "Icon_Banking"]
    var graphUnit = ["km", "h", "km/h", "m", "°"]
    
    @State var selectedGraph = 0
    @State var showDatePicker: Bool = false
    @State var savedDate: Date = Date()
    @State var records: [Any]? = nil
    @State var monthlyData: [[Double]]?
    @State private var selectedIndex: Int = Date().day - 1
    
    var body: some View {
        ZStack{
            NavigationView {
                ScrollView(.vertical) {
                    VStack {
                        ridingTotal()
                        
                        Section{
                            HStack{
                                Text(savedDate.toString(format: "MMMM dd, yyyy"))
                                    .foregroundColor(.blue)
                                    .font(.system(size: 16, weight: .bold))
                                
                                Spacer()
                                
                                Button(action: {showDatePicker.toggle()}, label: {Text("Show Calendar")})
                                    .font(.system(size: 10, weight: .medium))
                                    .padding(.horizontal)
                                    .frame(height: 18, alignment: .center)
                                    .background(Color(hex: 0x000000, alpha: 0.08))
                                    .cornerRadius(9)
                            }
                            .padding(.horizontal, 10)
                            .padding(.top, 30)
                            
                            Divider()
                                .frame(maxWidth: .infinity)
                                .frame(height: 4)
                                .background(Color.blue)
                            
                            VStack{
                                if monthlyData == nil {
                                    ProgressView()
                                } else {
                                    BarChartView(data: monthlyData![selectedGraph], primaryColor: Color(hex: 0x2F80ED), selectedIndex: $selectedIndex)
                                        .padding(.horizontal, 10)
                                        .padding(.vertical, 8)
                                }
                            }
                            .frame(height: max(UIScreen.main.bounds.width*0.52, 180), alignment: .center)
                            
                            HStack{
                                ForEach(0..<graph.count) { i in
                                    Button(action: {selectedGraph = i}, label: {
                                        HStack{
                                            Image(graphIcon[i])
                                                .resizable()
                                                .frame(width: 20, height: 20, alignment: .center)
                                                .foregroundColor(selectedGraph == i ? Color.white : Color.black)
                                            
                                            if selectedGraph == i {
                                                Text(graph[i])
                                                    .foregroundColor(Color.white)
                                            }

                                        }
                                        .frame(width: selectedGraph == i ? 105 : 26, height: 30, alignment: .center)
                                        .padding(.horizontal, 10)
                                        .background(selectedGraph == i ? Color.blue : Color(hex: 0x000000, alpha: 0))
                                        .cornerRadius(15)
                                    })
                                }
                            }
                            .padding(2)
                            .background(Color(hex: 0x000000, alpha: 0.08))
                            .cornerRadius(20)

                            HStack {
                                ForEach(0..<graph.count) { i in
                                    if i != 0 {
                                        Spacer()
                                    }
                                    VStack(spacing: 6) {
                                        Text(graph[i])

                                        Text("\((records == nil) ? "-" : "\(records![i])")")
                                            .font(.system(size: 20, weight: .bold))
                                            .foregroundColor(Color.blue)

                                        Text(graphUnit[i])
                                            .font(.system(size: 10, weight: .medium))
                                    }
                                }
                            }
                            .padding(.horizontal, 10)
                            .padding(.vertical, 20)
                        }
                        .font(.system(size: 12, weight: .bold))
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, 16)
                }
                .navigationBarColor(.black)
                .navigationBarTitleDisplayMode(.inline)
            }
                    
            if showDatePicker {
                CustomDatePicker(isShown: $showDatePicker, currentDate: $savedDate, records: $records, monthlyData: $monthlyData)
            }
        }
        .onAppear() {
            DispatchQueue.global(qos: .userInteractive).async {
                generalManager.getDailyRecord(date: savedDate) { res in
                    if let res = res {
                        records = [String(format: "%.1f", Float(res.distance)/1000), String(format: "%.1f", Float(res.time)/3600), res.bestSpeed, res.bestAltitude, res.bestBank]
                    }
                }
                generalManager.getMonthlyRecord(date: savedDate) { res in
                    monthlyData = res
                }
            }
        }
        .onChange(of: savedDate) { newValue in
            selectedIndex = newValue.day - 1
        }
    }
}

struct ridingTotal: View {
    @EnvironmentObject var generalManager: GeneralManager
    
    @State var overallDistance: Int = 0
    @State var overallTime: Int = 0
    
    var body: some View {
        VStack{
            ZStack{
                VStack{
                    Image("LogPage_Bike")
                        .resizable()
                        .frame(width: 48, height: 48, alignment: .center)
                    
                    Divider()
                        .frame(width: 1, height: 67)
                        .background(Color.gray)
                        .padding(.bottom, 20)
                }
                
                HStack(spacing: 35){
                    VStack(alignment: .center, spacing: 12){
                        Text("Total Distance")
                            .font(.system(size: 12, weight: .bold))
                        
                        VStack(alignment: .center, spacing: 5){
                            Text("\(String(format: "%.1f", Float(overallDistance)/1000))")
                                .font(.system(size: 24, weight: .black))
                            
                            Text("km")
                                .font(.system(size: 12, weight: .bold))
                                .foregroundColor(Color.gray)
                        }
                        .frame(maxWidth: .infinity)
                        .frame(height: 80)
                        .background(Color.white)
                        .cornerRadius(8)
                    }

                    VStack(alignment: .center, spacing: 12){
                        Text("Total Time")
                            .font(.system(size: 12, weight: .bold))
                        
                        VStack(alignment: .center, spacing: 5){
                            Text("\(String(format: "%.1f", Float(overallTime)/3600))")
                                .font(.system(size: 24, weight: .black))
                            
                            Text("hour")
                                .font(.system(size: 12, weight: .bold))
                                .foregroundColor(Color.gray)
                        }
                        .frame(maxWidth: .infinity)
                        .frame(height: 80)
                        .background(Color.white)
                        .cornerRadius(8)
                    }
                }
                .shadow(color: Color(hex: 0x000000, alpha: 0.12), radius: 8, y: 8)
                .offset(y: 20)
            }
        }
        .onAppear() {
            generalManager.getOverallData() { res in
                overallDistance = res.overallDistance
                overallTime = res.overallTime
            }
        }
    }
}

struct DashedLine: Shape {
        
    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: rect.width, y: rect.height))
        return path
    }
}

struct LogView_Previews: PreviewProvider {
    static var previews: some View {
        LogView()
    }
}
