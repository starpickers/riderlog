//
//  RidingLogList.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/01.
//

import SwiftUI

struct RidingDetailPopupModel: Identifiable {
    var id: String = UUID().uuidString
    var data: FBRiding
}

struct RidingLogList: View {
    @EnvironmentObject var generalManager: GeneralManager
    @State var ridingData: [FBRidings] = []
    @State var selectedRiding: RidingDetailPopupModel? = nil
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 0) {
                ForEach(ridingData.indices, id:\.self) { index in
                    DailyLogCell(selectedRiding: $selectedRiding, data: ridingData[index], prevDate: ridingData[max(0, index-1)].data.startAt, nextDate: ridingData[min(index+1, ridingData.count - 1)].data.startAt)
                }
            }
        }
        .popup(item: $selectedRiding) { i in
            ZStack{
                Color.black.opacity(0.1)
                
                VStack(spacing: 30) {
                    HStack(alignment: .top) {
                        Text(i.data.createdAt.toString(format: "MMMM dd, yyyy"))
                            .font(.system(size: 16, weight: .heavy))
                        
                        Spacer()
                        
                        Button {
                            selectedRiding = nil
                        } label: {
                            Image(systemName: "xmark")
                                .font(Font.system(size: 18, weight: .regular))
                        }
                        .offset(x: 4)

                    }
                    
                    RidingLogPopup(data: i.data)
                }
                .padding(20)
                .frame(width: UIScreen.main.bounds.width*0.8, height: UIScreen.main.bounds.height*0.62)
                .font(.system(size: 12, weight: .bold))
                .foregroundColor(.black)
                .background(Color.white)
                .cornerRadius(16)
                .shadow(color: Color(hex: 0xDFDFDF), radius: 16, x: 4, y: 8)
            }
        }
        .navigationBarColor(.black)
        .onAppear() {
            generalManager.getRidingsData() { res in
                ridingData = res
            }
        }
    }
}

struct DailyLogCell: View {
    @Binding var selectedRiding: RidingDetailPopupModel?
    
    var data: FBRidings
    var prevDate: Date
    var nextDate: Date
    
    var body: some View {
        VStack(alignment: .leading, spacing: 18) {
            if prevDate == data.data.startAt || prevDate.toString(format: "yyyy년 MM월 dd일") != data.data.startAt.toString(format: "yyyy년 MM월 dd일") {
                Text(data.data.startAt.toString(format: "MMMM dd, yyyy"))
                    .font(.system(size: 16, weight: .heavy))
            }
            
            VStack(spacing: 0) {
                LogCell(selectedRiding: $selectedRiding, isLast: nextDate == data.data.startAt || nextDate.toString(format: "yyyy년 MM월 dd일") != data.data.startAt.toString(format: "yyyy년 MM월 dd일"), data: data)
            }
            .padding(.horizontal, 6)
            
            if nextDate == data.data.startAt || nextDate.toString(format: "yyyy년 MM월 dd일") != data.data.startAt.toString(format: "yyyy년 MM월 dd일") {
                Divider()
            }
        }
        .padding(.horizontal, 20)
        .padding(.top, prevDate == data.data.startAt || prevDate.toString(format: "yyyy년 MM월 dd일") != data.data.startAt.toString(format: "yyyy년 MM월 dd일") ? 25 : 0)
    }
}

struct LogCell: View {
    @Binding var selectedRiding: RidingDetailPopupModel?
    
    var isLast: Bool
    var data: FBRidings
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0){
            HStack {
                Circle()
                    .stroke(Color(hex: 0xDADADA), lineWidth: 4)
                    .frame(width: 10, height: 10, alignment: .center)
                    .overlay(
                        Circle()
                            .foregroundColor(Color(hex: 0xFFFFFF))
                    )
                
                Text(data.data.startAt.toString(format: "h:mm a"))
                    .lineSpacing(0)
                    .font(.system(size: 12, weight: .regular))
            }

            HStack {
                if isLast {
                    VStack{ }
                        .frame(width: 10)
                } else {
                    HStack{
                        VerticalLine()
                            .stroke(Color(hex: 0xDADADA), lineWidth: 2)
                    }
                    .frame(idealHeight: 240, maxHeight: .infinity)
                    .frame(width: 10)
                }

                
                VStack(spacing: 14) {
                    HStack {
                        Text("Started At")
                        
                        Spacer()
                        
                        Text(data.data.startPlace)
                            .foregroundColor(Color(hex: 0x646464))
                    }
                    
                    HStack {
                        Text("Finished At")
                        
                        Spacer()
                        
                        Text(data.data.finishPlace)
                            .foregroundColor(Color(hex: 0x646464))
                    }
                    
                    HStack {
                        Text("Time")
                        
                        Spacer()
                        
                        Text("\(String(format: "%.1f", Float(data.data.totalTime)/3600)) H")
                            .foregroundColor(Color(hex: 0x646464))
                    }
                    
                    HStack {
                        Text("Distance")
                        
                        Spacer()
                        
                        Text("\(String(format: "%.1f", Float(data.data.totalDistance)/1000)) KM")
                            .foregroundColor(Color(hex: 0x646464))
                    }
                    
                    HStack {
                        Spacer()
                        
                        Button("More Details") {
                            selectedRiding = RidingDetailPopupModel(data: data.data)
                        }
                        .buttonStyle(CustomButton(width: 92, height: 24, fontWeight: .bold, fontSize: 10, fontColor: 0x000000, backgroundColor: 0x000000, radius: 25, type: .outlined))
                    }
                    .padding(.top, 8)
                }
                .font(.system(size: 12, weight: .bold))
                .foregroundColor(.black)
                .padding(24)
                .background(Color.white)
                .cornerRadius(12)
                .shadow(color: Color(hex: 0xDADADA, alpha: 0.6), radius: 10, x: 3, y: 5)
                .padding(.leading, 12)
                .padding(.top, 12)
                .padding(.bottom, 24)
            }
            .frame(idealHeight: 240, maxHeight: .infinity)
        }
    }
}


struct VerticalLine: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.maxY))
        return path
    }
}

struct RidingLogList_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            RidingLogList()
                .environmentObject(GeneralManager())
        }
    }
}
