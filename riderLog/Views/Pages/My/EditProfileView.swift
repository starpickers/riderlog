//
//  EditProfileView.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/01.
//

import SwiftUI
import Combine

struct EditProfileView: View {
    @EnvironmentObject var authViewModel: AuthViewModel
    @EnvironmentObject var generalManager: GeneralManager
    
    @State var tempImage: UIImage?
    
    @State var name: String = ""
    @State var nickname: String = ""
    @State var email: String = ""
    @State var phone: String = ""
    @State private var day: String = ""
    @State private var month: String = ""
    @State private var year: String = ""
    
    @State private var isLoading: Bool = false
    @State private var isSuccess: Bool? = nil
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 28) {
                AddProfileImageButton(image: $tempImage, color: Color(hex: 0xBABABA))
                    .scaleEffect(0.85)
                    .padding(.bottom, 15)
                
                VStack(alignment: .center, spacing: 22) {
                    EditableTextField(text: $name, title: "Name")
                    
                    EditableTextField(text: $nickname, title: "Nickname")
                    
                    EditableTextField(text: $email, title: "E-mail", isValid: { email in
                        authViewModel.isValidEmail(email)
                    }, errMsg: "올바른 이메일 주소를 입력 해 주세요.")

                    EditableTextField(text: $phone, title: "Phone", isValid: { phone in
                        authViewModel.isValidPhoneNumber(phone)
                    }, errMsg: "올바른 전화 번호를 입력 해 주세요.")
                        .keyboardType(.numberPad)
                        .onReceive(Just(phone)) { newValue in
                            let filtered = newValue.filter { "0123456789".contains($0) }
                            if filtered != newValue {
                                phone = filtered
                            }
                        }
                    
                    HStack {
                        Text("Birth")
                        
                        Spacer()
                        
                        ZStack{
                            HStack {
                                TextField("YYYY", text: $year)
                                    .padding(.vertical, 12)
                                    .padding(.horizontal, 18)
                                    .background(authViewModel.isValidDate("\(year)-\(month)-\(day)") != nil || year == "" || month == "" || day == "" ? Color(hex: 0xFAFAFA) : Color(hex: 0xFCF4F4))
                                    .cornerRadius(25)
                                
                                TextField("MM", text: $month)
                                    .padding(.vertical, 12)
                                    .padding(.horizontal, 18)
                                    .background(authViewModel.isValidDate("\(year)-\(month)-\(day)") != nil || year == "" || month == "" || day == "" ? Color(hex: 0xFAFAFA) : Color(hex: 0xFCF4F4))
                                    .cornerRadius(25)
                                
                                TextField("DD", text: $day)
                                    .padding(.vertical, 12)
                                    .padding(.horizontal, 18)
                                    .background(authViewModel.isValidDate("\(year)-\(month)-\(day)") != nil || year == "" || month == "" || day == "" ? Color(hex: 0xFAFAFA) : Color(hex: 0xFCF4F4))
                                    .cornerRadius(25)
      
                            }
                            .foregroundColor(Color(hex: 0x555555))
                            .keyboardType(.numberPad)
                            
                            if authViewModel.isValidDate("\(year)-\(month)-\(day)") == nil && ( year != "" || month != "" || day != "") {
                                HStack(spacing: 2) {
                                    Image(systemName: "exclamationmark.circle")
                                        .font(Font.system(size: 10, weight: .bold))
                                    
                                    Text("올바른 날짜를 입력해 주세요.")
                                        .font(Font.system(size: 10, weight: .bold))
                                    
                                    Spacer()
                                }
                                .foregroundColor(.red)
                                .offset(x: 10, y: 32)
                            }
                        }
                        .frame(width: 240)
                    }
                    
                    Button("SAVE") {
                        isLoading = true
                        if tempImage == nil {
                            authViewModel.saveUserInfo(email: email, name: name, nickname: nickname, birth: authViewModel.isValidDate("\(year)-\(month)-\(day)"), phone: phone)
                            authViewModel.setUser(merge: true, completionHandler: { isSuccess in
                                isLoading = false
                            })
                        } else {
                            generalManager.uploadImage(image: self.tempImage!, folder: "profiles", name: authViewModel.user.uid.uppercased() + "-PROFILE-IMAGE", completionHandler: { url in
                                authViewModel.saveUserInfo(email: email, name: name, nickname: nickname, birth: authViewModel.isValidDate("\(year)-\(month)-\(day)"), phone: phone, photoUrl: url.absoluteString)
                                authViewModel.setUser(merge: true, completionHandler: { isSuccess in
                                    isLoading = false
                                })
                            })
                        }
                    }
                    .buttonStyle(RegularButton(width: UIScreen.main.bounds.width - 50))
                    .padding(.vertical, 10)
                }
                .padding(.horizontal, 10)
                    
                Divider()
                
                HStack{
                    Button("Reset Password") {
                        DispatchQueue.global(qos: .userInteractive).async {
                            DispatchQueue.main.async {
                                isLoading = true
                            }
                            authViewModel.resetPassword(email: authViewModel.user.detail.email, completionHandler: { result in
                                isLoading = false
                                isSuccess = result
                            })
                        }
                    }
                    .font(.system(size: 13, weight: .bold))
                    
                    Spacer()
                    
                    if isSuccess != nil, isSuccess! {
                        Text("! Link sent successfully.")
                            .foregroundColor(.green)
                            .font(.system(size: 10, weight: .semibold))
                    } else if isSuccess != nil, !(isSuccess!) {
                        Text("! Unable to send link. Please try again.")
                            .foregroundColor(.red)
                            .font(.system(size: 10, weight: .semibold))
                    }
                }
                .padding(.horizontal, 10)
            }
            .padding(.vertical, 40)
            .padding(.horizontal, 10)
        }
        .popup(isPresented: $isLoading, content: {
            VStack(spacing: 18) {
                Text("Loading...")
                    .font(.system(size: 12, weight: .bold))
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle())
                    .scaleEffect(x: 1.4, y: 1.4, anchor: .center)
            }
            .frame(width: 140, height: 100, alignment: .center)
            .background(Color.white)
            .cornerRadius(16)
            .shadow(color: Color(hex: 0x000000, alpha: 0.3), radius: 12, x: 0, y: 0)
            .padding(.bottom, UIScreen.main.bounds.height/12)
        })
        .foregroundColor(.black)
        .font(.system(size: 13, weight: .medium))
        .navigationBarColor(.black)
        .navigationBarTitleDisplayMode(.inline)
        .onAppear() {
            DispatchQueue.global(qos: .userInitiated).async {
                name = authViewModel.user.detail.name
                nickname = authViewModel.user.detail.nickname
                email = authViewModel.user.detail.email
                phone = authViewModel.user.detail.phone
                if let birth = authViewModel.user.detail.birth {
                    year = birth.toString(format: "yyyy")
                    month = birth.toString(format: "MM")
                    day = birth.toString(format: "dd")
                }
            }
        }
    }
}

struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            EditProfileView()
                .environmentObject(AuthViewModel())
        }
    }
}
