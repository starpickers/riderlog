//
//  MyView.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/16.
//

import SwiftUI

struct MyView: View {
    @EnvironmentObject var alertData: AlertItemData
    @EnvironmentObject var generalManager: GeneralManager
    @EnvironmentObject var authViewModel: AuthViewModel
    
    @State private var profileImage: UIImage? = nil
    @State private var showingAlert: Bool = false
    @State private var showingCaliModal: Bool = false

    var body: some View {
        ZStack {
            NavigationView {
                ScrollView(.vertical) {
                    VStack (alignment: .leading, spacing: 25){
                        HStack (alignment: .top) {
                            VStack (spacing: 30) {
                                HStack(spacing: 24) {
                                    if profileImage == nil {
                                        if authViewModel.user.detail.photoUrl == nil {
                                            ZStack {
                                                Circle()
                                                    .strokeBorder(.gray, lineWidth: 1)
                                                    .frame(width: 90, height: 90, alignment: .center)
                                                
                                                Image(systemName: "person")
                                                    .foregroundColor(.gray)
                                                    .font(Font.system(size: 36, weight: .ultraLight))
                                            }
                                        } else {
                                            ZStack {
                                                Circle()
                                                    .strokeBorder(.gray, lineWidth: 1)
                                                    .frame(width: 90, height: 90, alignment: .center)
                                                
                                                ProgressView()
                                            }
                                        }
                                    } else {
                                        Image(uiImage: profileImage!)
                                            .resizable()
                                            .cornerRadius(50)
                                            .frame(width: 90, height: 90)
                                            .clipShape(Circle())
                                            .overlay(
                                                Circle()
                                                    .strokeBorder(.gray, lineWidth: 1)
                                            )
                                    }
                                    
                                    VStack(alignment: .leading, spacing: 10) {
                                        Text(authViewModel.user.detail.name)
                                            .font(.system(size: 18, weight: .bold))
                                        
                                        Text(authViewModel.user.detail.nickname)
                                            .font(.system(size: 14, weight: .bold))
                                    }
                                    
                                    Spacer()
                                }
                                
                                VStack(spacing: 16) {
                                    HStack(spacing: 20) {
                                        Image(systemName: "envelope.circle")
                                            .font(Font.system(size: 18, weight: .light))
                                        
                                        Text(authViewModel.user.detail.email)
                                        
                                        Spacer()
                                    }
                                    
                                    HStack(spacing: 20) {
                                        Image(systemName: "phone.circle")
                                            .font(Font.system(size: 18, weight: .light))
                                        
                                        Text(authViewModel.user.detail.phone)
                                        
                                        Spacer()
                                    }
                                }
                                .foregroundColor(Color(hex: 0x8E8E8E))
                                .padding(.horizontal, 4)
                            }
                            
                            Spacer()
                            
                            NavigationLink(destination: EditProfileView(tempImage: profileImage)) {
                                Image(systemName: "square.and.pencil")
                                    .font(Font.system(size: 20, weight: .regular))
                            }
                        }
                        .padding(.top, 25)
                        .padding(.horizontal, 12)
                        
                        Divider()
                        
                        NavigationLink(destination: RidingLogList()) {
                            HStack{
                                Text("My Riding Log")
                                    .font(.system(size: 16, weight: .heavy))
                                
                                Spacer()
                                
                                Image("MyPage_Arrow")
                                    .rotationEffect(Angle(degrees: -90))
                            }
                            .padding(.horizontal, 12)
                        }

                        Divider()
                        
                        VStack(alignment: .leading, spacing: 28) {
                            Text("Sensor")
                                .font(.system(size: 16, weight: .heavy))
                            
                            HStack {
                                Text("Name")
                                
                                Spacer()
                                
                                Text("RIDERLOG-1234")
                                    .foregroundColor(Color(hex: 0x8E8E8E))
                            }
                            
                            HStack {
                                Text("Version")
                                
                                Spacer()
                                
                                Text(generalManager.sensor.data.FIRM_VER)
                                    .foregroundColor(Color(hex: 0x8E8E8E))
                            }
                            
                            Button("Calibration") {
                                showingCaliModal = true
                            }
                            
                            Button("Reboot") {
                                generalManager.requestReboot()
                            }
                            
                            Button("Reset") {
                                generalManager.requestFactoryReset()
                            }
                            
                            Button("Firmware Update") {
                                generalManager.requestFirmUpdate()
                            }
                        }
                        .padding(.horizontal, 12)
                        
                        Divider()
                        
                        VStack(alignment: .leading, spacing: 28) {
                            HStack {
                                Text("Version")
                                
                                Spacer()
                                
                                Text("1.0.0")
                                    .foregroundColor(Color(hex: 0x8E8E8E))
                            }
                            
                            Button("Sign Out") {
                                authViewModel.signOut()
                            }
                            .foregroundColor(.red)
                            .font(.system(size: 14, weight: .bold))
                        }
                        .padding(.horizontal, 12)
                        
                        Spacer()
                    }
                    .foregroundColor(.black)
                    .padding(.horizontal, 15)
                }
                .font(.system(size: 14, weight: .medium))
                .navigationBarColor(.black)
                .navigationBarTitleDisplayMode(.inline)
            }
        }
        .onAppear() {
            DispatchQueue.global(qos: .userInteractive).async {
                if let url = authViewModel.user.detail.photoUrl {
                    profileImage = url.getImage()
                }
            }
        }
        .actionSheet(isPresented: $showingCaliModal) {
            ActionSheet(title: Text("Select a category to be calibrated"), buttons: [
                .default(Text("Gyroscope")) {
                    generalManager.requestCalibrateGyro()
                },
                .default(Text("Acceleration")) {
                    generalManager.requestCalibrateAcc()
                },
                .default(Text("Origin")) {
                    generalManager.requestSetOrigin()
                },
                .default(Text("All")) {
                    generalManager.requestCalibrateAll()
                },
                .cancel()
            ])
        }
        .onChange(of: showingAlert) {_ in
            if showingAlert == true {
                self.alertData.alertItem = AlertItem(title: Text("ERROR"), message: Text("센서를 먼저 연결해 주세요"), dismissButton: .default(Text("OK"), action: {showingAlert = false}))
            }
        }
        .onChange(of: generalManager.caliResult) {_ in
            switch generalManager.caliResult {
            case 1:
                self.alertData.alertItem = AlertItem(title: Text("SUCCESS"), message: Text("센서 보정 완료"), dismissButton: .default(Text("OK"), action: {generalManager.caliResult = 0}))
                break
            case 2:
                self.alertData.alertItem = AlertItem(title: Text("FAIL"), message: Text("센서 보정 실패"), dismissButton: .default(Text("OK"), action: {generalManager.caliResult = 0}))
                break
            default:
                break
            }
        }
    }
}

struct MyView_Previews: PreviewProvider {
    static var previews: some View {
        MyView()
    }
}
