//
//  NavigateView.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/16.
//

import SwiftUI

struct NavigateView: View {
    @StateObject var viewRouter = ViewRouter()
    @EnvironmentObject var generalManager: GeneralManager
    
    @State private var showActionSheet: Bool = false
    @State private var showScanList: Bool = false
    @State private var showTabBar: Bool = false
    @State private var showResult: Bool = false
    
    @State private var offset = CGSize.zero
  
    var body: some View {
        ZStack(alignment:.top){
            VStack(spacing: 0) {
                switch viewRouter.currentPage {
                 case .riding:
                    MainView(showActionSheet: $showActionSheet, showScanList: $showScanList, showTabBar: $showTabBar, showResult: $showResult)
                 case .log:
                    LogView()
                        .onAppear() {
                            if !showTabBar {
                                showTabBar = true
                            }
                        }
                 case .my:
                    MyView()
                        .onAppear() {
                            if !showTabBar {
                                showTabBar = true
                            }
                        }
                 }
                
                ZStack{
                    if !showScanList && showTabBar {
                        HStack {
                            TabBarElement(viewRouter: viewRouter, assignedPage: .riding, image: "Riding", label: "라이딩")
                                .frame(maxWidth: .infinity)
                            TabBarElement(viewRouter: viewRouter, assignedPage: .log, image: "Challenge", label: "주행기록")
                                .frame(maxWidth: .infinity)
                            TabBarElement(viewRouter: viewRouter, assignedPage: .my, image: "My", label: "내 정보")
                                .frame(maxWidth: .infinity)
                         }
                        .padding(.bottom, (UIApplication.shared.windows.first?.safeAreaInsets.bottom)!)
                        .padding(.horizontal, 20)
                        .frame(width: UIScreen.main.bounds.width, height: 78 + (UIApplication.shared.windows.first?.safeAreaInsets.bottom)!)
                        .background(Color(hex: 0x000000, alpha: 0.9))
                    }
                }
                .ignoresSafeArea(.keyboard, edges: .bottom)
            }
            .ignoresSafeArea(.container)
            
            if !showResult {
                Image("Logo")
                    .resizable()
                    .frame(width: 102, height: 33)
                    .padding(.top, showActionSheet ? (UIApplication.shared.windows.first?.safeAreaInsets.top)! - 33/4 : 0)
            }
            
            CustomActionSheet(isShowing: $showActionSheet, primaryBtnText: generalManager.riderLog.recordState == .START ? "일시 정지" : "라이딩 재개", primaryBtnFunc: generalManager.riderLog.recordState == .START ? {generalManager.pauseREC()} : {generalManager.startREC()}, secondaryBtnText: "라이딩 종료", secondaryBtnFunc: {
                generalManager.stopREC()
                showResult = true
            })

            /// Ecall 알람
            if generalManager.emergencyCall == .detect {
                EcallAlert()
            }
            
            /// 데이터 복구 모달
            if restoredData, generalManager.riderLog.recordState == .STOP, let data = UserDefaults.standard.value(forKey:"unsaved riding") as? Data {
                let ridingData = try? PropertyListDecoder().decode(RidingDetail.self, from: data)
                if let data = ridingData {
                    SaveRidingModal(data: data)
                }
            }
        }
    }
}

struct TabBarElement: View {
    @StateObject var viewRouter: ViewRouter
    
    let assignedPage: Page
    let image, label: String
    var isActive = true

    var body: some View {
        
        ZStack {
            Image(image)
                .resizable()
                .frame(width: 28, height: 28, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .foregroundColor(viewRouter.currentPage == assignedPage ? Color.white : Color(hex: 0xffffff, alpha: 0.5))
                .offset(y: -4)
            
            RoundedRectangle(cornerRadius: 4)
                .fill(viewRouter.currentPage == assignedPage ? Color(hex: 0xEB5757) : Color(hex: 0xEB5757, alpha: 0))
                .frame(width: 32, height: 4, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .padding(.vertical, 8)
                .offset(y: 20)
        }
        .frame(width: (UIScreen.main.bounds.width-30)/5, height: 54, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .onTapGesture {
            viewRouter.currentPage = assignedPage
        }
        .foregroundColor(viewRouter.currentPage == assignedPage ? Color.white : .gray)
    }
}

struct NavigateView_Previews: PreviewProvider {    
    static var previews: some View {
        NavigateView(viewRouter: ViewRouter())
    }
}

