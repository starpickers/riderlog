//
//  ViewRouter.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/16.
//

import SwiftUI

class ViewRouter: ObservableObject {
    @Published var currentPage: Page = .riding
}

enum Page {
     case riding
     case log
     case my
 }
