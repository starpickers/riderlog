//
//  Sensor.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/07.
//

import Foundation
import CoreBluetooth

struct Sensor {
    var isConnected: Bool
    var data: Sensor.Data
}

extension Sensor {
    struct Data {
        var FIRM_VER: String
        var GPSDATA: [Float] /// [ 위도, 경도, 속도, 고도, 이동거리 ]
        var IMUDATA: [Int]
        var ATTDATA: [Float]
        var EVENTS: [Bool]
    }
    
    struct Detail: Identifiable, Equatable {
        let id: Int
        let peripheral: CBPeripheral
        let name: String
        var rssi: Int
    }
}

extension Sensor.Data {
    init() {
        self.FIRM_VER = "-"
        self.GPSDATA = [Float](repeating: 0, count: 5)
        self.IMUDATA = [Int](repeating: 0, count: 6)
        self.ATTDATA = [Float](repeating: 0, count: 2)
        self.EVENTS = [Bool](repeating: false, count: 4)
    }
}

extension Sensor {
    /// MSG ID
    static let SENSOR_MSG_CHECK_CONNECTION = 0x0000;
    static let SENSOR_MSG_REQUEST_FIRM_VERSION = 0x0002;
    static let SENSOR_MSG_REQUEST_REBOOT = 0x0004;
    static let SENSOR_MSG_REQUEST_ALL_CALI = 0x0010;
    static let SENSOR_MSG_REQUEST_ACCEL_CALI = 0x0011;
    static let SENSOR_MSG_REQUEST_GYRO_CALI = 0x0012;
    static let SENSOR_MSG_REQUEST_ORIGIN_CALI = 0x0014;
    
    static let SERVER_MSG_REQUEST_USER_ID = 0x0010;
    static let SERVER_MSG_REQUEST_USER_NAME = 0x0011;
    static let SERVER_MSG_REQUEST_USER_EMAIL = 0x0012;
    static let SERVER_MSG_REQUEST_USER_MOBILE = 0x0013;

    static let SENSOR_MSG_IMU = 0x0100;
    static let SENSOR_MSG_ATT = 0x0101;
    static let SENSOR_MSG_GNSS = 0x0102;
    static let SENSOR_MSG_MAG = 0x0103;
    static let SENSOR_MSG_TEMP = 0x0104;
    static let SENSOR_MSG_BARO = 0x0105;
    
    static let ETC_MSG_EVENTS = 0x0201;
    
    /// UUID
    static let SERVICE_UUID: String = "6e400001-b5a3-f393-e0a9-e50e24dcca9e"
    static let CHARACTER_TX_UUID: String = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
    static let CHARACTER_RX_UUID: String = "6e400003-b5a3-f393-e0a9-e50e24dcca9e"
    static let DESCRIPTOR_UUID: String = "000002902-0000-1000-8000-00805f9b34fb"
    
    /// Request data
    static let REQUEST_IMU_DATA = [0xFF, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_ATT_DATA = [0xFF, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_FIRM_VERSION_DATA = [0xFF, 0x00, 0x02, 0x03, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_FIRM_UPDATE_DATA = [0xFF, 0x00, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_REBOOT_DATA = [0xFF, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_CALI_ALL_DATA = [0xFF, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_CALI_ACC_DATA = [0xFF, 0x00, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_CALI_GYRO_DATA = [0xFF, 0x00, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_SET_ORIGIN_DATA = [0xFF, 0x00, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00]
    static let REQUEST_FACTORY_RESET_DATA = [0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00]
}
