//
//  Auth.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/06.
//

import Foundation

struct Credentials {
    enum AuthenticationState {
        case signUp, signIn, signOut
    }
    var state: AuthenticationState
    
    var uid: String
    var detail: Credentials.Detail
}

extension Credentials {
    init() {
        self.state = .signOut
        
        self.uid = ""
        self.detail = Credentials.Detail()
    }
}

extension Credentials {
    struct Detail: Codable, Equatable {
        var email: String = ""
        var name: String = ""
        var phone: String = ""
        var nickname: String = ""
        
        var birth: Date?
        var photoUrl: String?
        
        var createdAt: Date?
    }
}
