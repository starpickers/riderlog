//
//  DataStructrue.swift
//  riderLog
//
//  Created by 박서현 on 2021/10/29.
//

import Foundation

//MARK: FIREBASE
struct FBRidings: Identifiable {
    var id: String
    var data: FBRiding
}

struct FBRiding: Codable {
    var bestAltitude: Int
    var bestBank: Int
    var bestSpeed: Int
    var comment: String
    var createdAt: Date
    var finishAt: Date
    var finishPlace: String
    var photoUrls: [String]
    var startAt: Date
    var startPlace: String
    var totalDistance: Int
    var totalTime: Int
    var userId: String
}

struct FBOverallData: Codable {
    var overallDistance: Int
    var overallTime: Int
}

struct FBDailyRecord: Codable {
    var bestAltitude: Int
    var bestBank: Int
    var bestSpeed: Int
    var createdAt: Date
    var distance: Int
    var time: Int
}

//MARK: REVERSE GEOCOING
struct RGResponse: Codable {
    let status: RGResponseStatus
    let results: [RGResult]
    
    enum CodingKeys: String, CodingKey {
        case status
        case results
    }
}

struct RGResponseStatus: Codable {
    let code: Int
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case code
        case message
    }
}

struct RGResult: Codable {
    let region: RGLocateDetailWrapper
    let land: RGOptionalLocateInfo?
    let addition0: RGAdditionalLocateInfo?

    enum CodingKeys: String, CodingKey {
        case region
        case land
        case addition0
    }
}

struct RGLocateDetailWrapper: Codable {
    let area1: RGLocateDetail
    let area2: RGLocateDetail

    enum CodingKeys: String, CodingKey {
        case area1
        case area2
    }
}

struct RGOptionalLocateInfo: Codable {
    let name: String
    let number1: String
    
    enum CodingKeys: String, CodingKey {
        case name
        case number1
    }
}

struct RGAdditionalLocateInfo: Codable {
    let type: String
    let value: String
    
    enum CodingKeys: String, CodingKey {
        case type
        case value
    }
}

struct RGLocateDetail: Codable {
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case name
    }
}
