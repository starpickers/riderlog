//
//  Riding.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/07.
//

import Foundation

struct RidingRecord {
    enum RecordState {
        case start, pause, stop
    }
    var recordState: RecordState = .stop
    var userID: String? = nil
    var currentRiding: RidingRecord.Detail
}

extension RidingRecord {
    struct Detail: Codable, Equatable {
        var cteatedAt: Date?
        var ridingID: String?
        
        var startPlace: String
        var finishPlace: String
        var totalDistance: String
        
        var startAt: Date
        var finishAt: Date
        var totalTime: Int
        
        var bestAltitude: Int
        var bestBank: Int
        var bestSpeed: Int
        
        var comment: String
        
        var photoUrls: [String]?
    }
}
