//
//  AuthViewModel.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/12.
//

import SwiftUI
import GoogleSignIn
import Firebase
import FirebaseAuth

extension String {
    func getImage() -> UIImage? {
        guard let url = URL(string: self) else { return nil }
        do {
            let data = try Data(contentsOf: url)
            return UIImage(data: data)
        } catch {
            print(error)
            return nil
        }
    }
}

enum NavigationHelper {
    case throughSNS, throughEmail
}

class AuthViewModel: ObservableObject {
    @Published var user = Credentials()
    @Published var isLoading: Bool = false
    
    @Published var selectionView: NavigationHelper? = nil

    //MARK: 로그인 및 회원가입
    /*
     SNS - apple, google
     */
    func createUser(with credential: AuthCredential, completionHandler: ((Bool, Error?) -> Void)? = nil) {
        guard let defaultApp = FirebaseApp.app(name: "__FIRAPP_DEFAULT") else {
            assert(false, "Could not retrieve default app")
            return
        }
        isLoading = true
        Auth.auth(app: defaultApp).signIn(with: credential) { authResult, error in
            self.handleResult(authResult: authResult, error: error, completionHandler: completionHandler)
        }
    }
    
    func signInWithGoogle(completionHandler: @escaping (Bool, Error?) -> Void) {
        guard let clientID = FirebaseApp.app()?.options.clientID else { return }
        let config = GIDConfiguration(clientID: clientID)
        
        GIDSignIn.sharedInstance.signIn(with: config, presenting: (UIApplication.shared.windows.first?.rootViewController)!) { user, error in
            if let error = error {
                print("!Error: " + error.localizedDescription)
                return
            }
            
            guard let authentication = user?.authentication, let idToken = authentication.idToken else { return }
            
            let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: authentication.accessToken)
            
            self.createUser(with: credential, completionHandler: completionHandler)
        }
    }

    
    /*
     Email
     */
    func createUser(email: String, password: String, completionHandler: @escaping (Bool, Error?) -> Void) {
        guard let defaultApp = FirebaseApp.app(name: "__FIRAPP_DEFAULT") else {
            assert(false, "Could not retrieve default app")
            return
        }
        isLoading = true
        Auth.auth(app: defaultApp).createUser(withEmail: email, password: password) { authResult, error in
            self.handleResult(authResult: authResult, error: error, completionHandler: completionHandler)
        }
    }
    
    func signIn(email: String, password: String, completionHandler: @escaping (Bool, Error?) -> Void) {
        guard let defaultApp = FirebaseApp.app(name: "__FIRAPP_DEFAULT") else {
            assert(false, "Could not retrieve default app")
            return
        }
        isLoading = true
        Auth.auth(app: defaultApp).signIn(withEmail: email, password: password) { authResult, error in
            self.handleResult(authResult: authResult, error: error, completionHandler: completionHandler)
        }
    }
    
    func resetPassword(email: String, completionHandler: @escaping (Bool) -> Void) {
        guard let defaultApp = FirebaseApp.app(name: "__FIRAPP_DEFAULT") else {
            assert(false, "Could not retrieve default app")
            return
        }
        isLoading = true
        Auth.auth(app: defaultApp).sendPasswordReset(withEmail: email) { error in
            if let error = error {
                completionHandler(false)
                print("!Authentication error: \(error.localizedDescription)")
                return
            }
            
            completionHandler(true)
            self.isLoading = false
        }
    }
    
    func handleResult(authResult: AuthDataResult?, error: Error?, completionHandler: ((Bool, Error?) -> Void)? = nil) {
        if let error = error {
            isLoading = false
            if let _ = completionHandler {
                completionHandler!(false, error)
            }
            print("!Authentication error: \(error)")
            return
        }
        
        guard let result = authResult, let userInfo = result.additionalUserInfo else { return }
        
        self.user.uid = result.user.uid

        if userInfo.isNewUser {
            self.user.detail.email = result.user.email ?? ""
            self.user.detail.name = result.user.displayName ?? ""
            
            self.user.state = .signUp
            switch userInfo.providerID {
            case "apple.com":
                selectionView = .throughSNS
            case "google.com":
                selectionView = .throughSNS
            default:
                selectionView = .throughEmail
            }
        } else {
            self.user.state = .signIn
        }
        
        if let _ = completionHandler {
            completionHandler!(true, nil)
        }
        isLoading = false
        print("!Successfully sign-in with \(result.user.uid)")
    }
    
    func saveUserInfo(email: String? = nil, name: String? = nil, nickname: String? = nil, birth: Date? = nil, phone: String? = nil, photoUrl: String? = nil) {
        if let email = email { user.detail.email = email }
        if let name = name { user.detail.name = name }
        if let nickname = nickname { user.detail.nickname = nickname }
        if let birth = birth { user.detail.birth = birth }
        if let phone = phone { user.detail.phone = phone }
        if let photoUrl = photoUrl { user.detail.photoUrl = photoUrl }
    }
    
    
    //MARK: 로그아웃 및 계정 삭제
    func signOut() {
        if let providerData = Auth.auth().currentUser?.providerData {
            for userInfo in providerData {
                switch userInfo.providerID {
                case "apple.com":
                    UserDefaults.standard.set(nil, forKey: "appleAuthorizedUserIdKey")
                case "google.com":
                    GIDSignIn.sharedInstance.signOut()
                default:
                    print("user is signed in with \(userInfo.providerID)")
                }
            }
        }
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print("!Error signing out: %@", signOutError)
        }
        
        self.user = Credentials()
        self.user.state = .signOut
        isSignedIn = false
    }
    
    func deleteAccount() {
        selectionView = nil
        if let user = Auth.auth().currentUser {
            user.delete { error in
                if let error = error {
                    print("!Error deleting account: \(error.localizedDescription)")
                } else {
                    print("!Successfully deleted user account: \(user.uid)")
                    ///Initialize user data
                    self.user = Credentials()
                }
            }
        } else {
            print("!Error: Cannot found current user. Please sign-in before trying to delete account")
        }
    }
    
    //MARK: 유효성 검사
    func isValidEmail(_ str: String) -> Bool {
        let emailReg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailReg)
        return emailTest.evaluate(with: str) || str == ""
    }
    
    func isValidPassword(_ str: String) -> Bool {
        let passwordReg = ("(?=.*[A-Za-z])(?=.*[0-9]).{8,20}")
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordReg)
        return passwordTest.evaluate(with: str) || str == ""
    }
    
    func isValidDate(_ str: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")

        if let date = dateFormatter.date(from: str) { return date } else { return nil }
    }

    func isValidPhoneNumber(_ str: String) -> Bool {
        let phonenumberReg = ("(?=.*[0-9]).{10,11}")
        let phonenumberTest = NSPredicate(format:"SELF MATCHES %@", phonenumberReg)
        return phonenumberTest.evaluate(with: str) || str == ""
    }

    
    //MARK: GET / POST
    func getUserInfo(completionHandler: @escaping (Bool) -> Void) {
        guard let defaultApp = FirebaseApp.app(name: "__FIRAPP_DEFAULT") else {
            assert(false, "Could not retrieve default app")
            return
        }
        
        guard let user = Auth.auth(app: defaultApp).currentUser else { return }
        
        self.user.uid = user.uid
        
        Firestore.firestore(app: defaultApp).collection("users").document(user.uid).getDocument() { document, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }

            if let document = document, document.exists {
                do {
                    if let data = try document.data(as: Credentials.Detail.self) {
                        self.user.detail = data
                        completionHandler(true)
                    }
                } catch {
                    print("error" + String(describing: error))
                }
            }
        }
    }
    
    func setUser(merge: Bool, completionHandler: @escaping (Bool) -> Void) {
        guard let defaultApp = FirebaseApp.app(name: "__FIRAPP_DEFAULT") else {
            assert(false, "Could not retrieve default app")
            return
        }

        if merge {
            let data: [String: Any?] = [
                "birth": user.detail.birth,
                "nickname": user.detail.nickname,
                "email": user.detail.email,
                "phone": user.detail.phone,
                "name": user.detail.name,
                "photoUrl": user.detail.photoUrl
            ]
            
            Firestore.firestore(app: defaultApp).collection("users").document(user.uid).setData(data, merge: merge) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                    completionHandler(false)
                } else {
                    print("Document successfully updated")
                    completionHandler(true)
                }
            }
        } else {
            let data: [String: Any?] = [
                "birth": user.detail.birth,
                "createdAt": FieldValue.serverTimestamp(),
                "nickname": user.detail.nickname,
                "email": user.detail.email,
                "phone": user.detail.phone,
                "name": user.detail.name,
                "photoUrl": user.detail.photoUrl
            ]
            
            Firestore.firestore(app: defaultApp).collection("users").document(user.uid).setData(data, merge: merge) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                    completionHandler(false)
                } else {
                    print("Document successfully updated")
                    completionHandler(true)
                }
            }
        }
    }
    
}
