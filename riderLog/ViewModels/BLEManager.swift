//
//  BLEManager.swift
//  riderLog
//
//  Created by 박서현 on 2021/09/27.
//

import Foundation
import CoreBluetooth

class BLEManager: NSObject, ObservableObject {
    @Published var peripherals = [Sensor.Detail]()
    
    private var centralManager: CBCentralManager!
    private var peripheralManager: CBPeripheral!
    
    private var connectedPeripheral: CBPeripheral?
    private var bluetoothGattCharacteristic_rx: CBCharacteristic!
    private var bluetoothGattCharacteristic_tx: CBCharacteristic!
    
    public var requestCali: Bool = false
    
    func startService() {
        centralManager = CBCentralManager(delegate: self, queue: nil)
        centralManager.delegate = self
    }

    func writeData(data: [Int]) {
        var sendData: [UInt8] = [UInt8](repeating: 0, count: data.count)
        for i in 0..<data.count {
            sendData[i] = UInt8(data[i])
        }
        let data = Data(sendData)
        if let connectedPeripheral = connectedPeripheral {
            if let tx = bluetoothGattCharacteristic_tx {
                connectedPeripheral.writeValue(data, for: tx, type: .withoutResponse)
            }
        }
    }
    
    func requestIMU() {
        /// IMU 데이터 요청 함수
        writeData(data: Sensor.REQUEST_IMU_DATA)
    }
    
    func requestAttitude() {
        /// ATT 데이터 요청 함수
        writeData(data: Sensor.REQUEST_ATT_DATA)
    }
    
    func requestFirmVersion() {
        /// 펌웨어 버전 요청 함수
        writeData(data: Sensor.REQUEST_FIRM_VERSION_DATA)
    }
    
    func requestFirmUpdate() {
        /// 펌웨어 버전 업데이트 함수
        writeData(data: Sensor.REQUEST_FIRM_UPDATE_DATA)
    }
    
    func requestReboot() {
        /// 재부팅 요청
        writeData(data: Sensor.REQUEST_REBOOT_DATA)
    }
    
    func requestCalibrateAll() {
        /// 모든 센서 보정 요청
        writeData(data: Sensor.REQUEST_CALI_ALL_DATA)
    }

    func requestCalibrateAcc() {
        /// 가속도 보정 요청
        writeData(data: Sensor.REQUEST_CALI_ACC_DATA)
    }

    func requestCalibrateGyro() {
        /// 자이로 보정 요청
        writeData(data: Sensor.REQUEST_CALI_GYRO_DATA)
    }
    
    func requestSetOrigin() {
        /// 원점 설정 요청
        writeData(data: Sensor.REQUEST_SET_ORIGIN_DATA)
    }

    func requestFactoryReset() {
        /// 공장 초기화 요청
        writeData(data: Sensor.REQUEST_FACTORY_RESET_DATA)
    }
    
    func startScan() {
        print("scanning for new peripherals with service \(Sensor.SERVICE_UUID)")
        
        peripherals = [Sensor.Detail]()

        /// 해당 serviece를 포함한 peripherals만 검색
        centralManager.scanForPeripherals(withServices: [CBUUID(string: Sensor.SERVICE_UUID)], options: nil)
    }
    
    func stopScan() {
        print("stopped scanning for new peripherals")
        centralManager.stopScan()
    }
    
    func connect(to peripheral: CBPeripheral) {
        print("connecting to \(peripheral.name ?? "unnamed peripheral")")
        peripheral.delegate = self
        centralManager.connect(peripheral, options: nil)
    }
    
    func connect(to identifier: String) {
        for peripheral in centralManager.retrievePeripherals(withIdentifiers: [UUID(uuidString: identifier)!]) {
            print("connecting to retrieved peripheral \(peripheral.name ?? "unnamed peripheral")")
            centralManager.connect(peripheral, options: nil)
        }
    }
    
    func disconnect() {
        if let connectedPeripheral = connectedPeripheral {
            centralManager.cancelPeripheralConnection(connectedPeripheral)
        }
        connectedPeripheral = nil
    }
    
    func reconnect() {
        if let connectedPeripheral = connectedPeripheral {
            centralManager.connect(connectedPeripheral, options: nil)
        }
    }
}

// MARK: CBCentralManagerDelegate

extension BLEManager: CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        /// 0: unknown  |  1: resetting  |  2: unsupported  |  3: unauthorized  |  4: poweredOff  |  5: poweredOn
        let state = central.state
        print("central state is: \(state.rawValue)")
        
        if state == .poweredOn {
            DispatchQueue.global().async {
                self.centralManager.scanForPeripherals(withServices: [CBUUID(string: Sensor.SERVICE_UUID)], options: nil)
                Thread.sleep(forTimeInterval: 1)
                
                /// 저장된 센서가 있을 시 자동 연결
                if let id = UserDefaults.standard.string(forKey: "PairBLE") {
                    self.connect(to: id)
                }
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        var isAdded: Bool = false
        
        if let name = peripheral.name {
            
            let newPeripheral = Sensor.Detail(id: peripherals.count, peripheral: peripheral, name: name, rssi: RSSI.intValue)
            
            for peripheral in peripherals {
                if peripheral.name == name {
                    isAdded = true
                    break
                }
            }
            
            if !isAdded {
                print("discovered \(peripheral.name ?? "unnamed peripheral")")
                peripherals.append(newPeripheral)
            }
            /*list안에 추가되지 않은 peripheral만 추가 (advertisement data(그 중 rssi value)가 변할 때마다 다른 peripheral로 인식)
             -> advertisement data가 변하면 기존 list에 반영되도록 수정 필요*/
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        connectedPeripheral = peripheral
        
        ///연결된 블루투스 센서 정보 저장
        UserDefaults.standard.set(peripheral.identifier.uuidString, forKey: "PairBLE")
        
        print("Connected to \(peripheral.name ?? "unnamed peripheral")")
        NotificationCenter.default.post(name: Notification.Name("BLE_CONNECTION_STATUS"), object: true)
        
        if let connectedPeripheral = connectedPeripheral {
            connectedPeripheral.delegate = self
            connectedPeripheral.discoverServices([CBUUID(string: Sensor.SERVICE_UUID)])
        }
        
        stopScan()
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("failed to connect to \(peripheral.name ?? "unnamed peripheral") \(error != nil ? "with error: \(error!.localizedDescription)" : "")")
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("disconnected from \(peripheral.name ?? "unnamed peripheral") \(error != nil ? "with error: \(error!.localizedDescription)" : "")")
        NotificationCenter.default.post(name: Notification.Name("BLE_CONNECTION_STATUS"), object: false)
        
        if requestCali == true {
            reconnect()
            requestCali = false
        }
    }
}

// MARK: CBPeripheralDelegate
extension BLEManager: CBPeripheralDelegate {
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let error = error {
            print("error discovering service: \(error.localizedDescription)")
            return
        }
        
        guard let service = peripheral.services?.first(where: { $0.uuid == CBUUID(string: Sensor.SERVICE_UUID) }) else {
            print("no valid services on \(peripheral.name ?? "unnamed peripheral")")
            return
        }
        
        print("discovered service \(service.uuid) on \(peripheral.name ?? "unnamed peripheral")")
        peripheral.discoverCharacteristics(nil, for: service)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        if invalidatedServices.contains(where: { $0.uuid == CBUUID(string: Sensor.SERVICE_UUID)}) {
            print("\(peripheral.name ?? "unnamed peripheral") did invalidate service \(CBUUID(string: Sensor.SERVICE_UUID))")
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        if let error = error {
            print("error discovering characteristic: \(error.localizedDescription)")
            return
        }
        
        guard let characteristics = service.characteristics, !characteristics.isEmpty else {
            print("no characteristics discovered on \(peripheral.name ?? "unnamed peripheral") for service \(service.description)")
            return
        }

        if let characteristic = characteristics.first(where: {$0.uuid == CBUUID(string: Sensor.CHARACTER_RX_UUID)}) {
            bluetoothGattCharacteristic_rx = characteristic
            print("discovered characteristic_rx \(characteristic.uuid) on \(peripheral.name ?? "unnamed peripheral")")
            
            /// Subscribe to this characteristic
            peripheral.setNotifyValue(true, for: characteristic)
            
            if let descriptor = characteristic.descriptors?.first(where: {$0.uuid == CBUUID(string: Sensor.DESCRIPTOR_UUID)}) {
                print("discovered descriptor \(descriptor.uuid) on \(peripheral.name ?? "unnamed peripheral")")
            }
        }

        if let characteristic = characteristics.first(where: {$0.uuid == CBUUID(string: Sensor.CHARACTER_TX_UUID)}) {
            bluetoothGattCharacteristic_tx = characteristic
            print("discovered characteristic_tx \(characteristic.uuid) on \(peripheral.name ?? "unnamed peripheral")")
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            print("\(characteristic.uuid) on \(peripheral.name ?? "unnamed peripheral") failed to update notification state with error: \(error.localizedDescription)")
            return
        }
        
        if characteristic.isNotifying {
            print("\(characteristic.uuid) on \(peripheral.name ?? "unnamed peripheral") notification have begun")
            requestFirmVersion()
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            print("\(peripheral.name ?? "unnamed peripheral") failed to update value with error: \(error.localizedDescription)")
            return
        }

        if characteristic.uuid == CBUUID(string: Sensor.CHARACTER_RX_UUID) {
            guard let data = characteristic.value else {
                print("characteristic value from \(peripheral.name ?? "unnamed peripheral") is nil")
                return
            }
            
            var header = [Int](repeating: 0, count: 8)
            var payload = [Int](repeating: 0, count: 247)
            
            for i in 0..<8 {
                header[i] = Int(data[i])
            }
            
            let message_id: Int = Int(Int32(bitPattern: UInt32((header[1] << 8) | header[2])))
            let message_device_id: Int = Int(Int32(bitPattern: UInt32((header[4] << 24) | (header[5] << 16) | (header[6] << 8) | header[7])))
            for i in 0..<header[3] {
                payload[i] = Int(data[i+8])
            }
            
            NotificationCenter.default.post(name: Notification.Name("BLE_NOTIFY_DATA"), object: ["MSG_ID": message_id, "MSG_DEVICE_ID": message_device_id, "MSG_PAYLOAD": payload])
        }
    }
}
