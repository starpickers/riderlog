//
//  TimeCounter.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/26.
//

import Foundation
import Combine

class TimeCounter: ObservableObject {
    @Published var time: Int = 0
    @Published var timerIsPaused: Bool = true
    
    @Published var timer: Timer?
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1.0,
                                     target: self,
                                     selector: #selector(timerDidFire),
                                     userInfo: nil,
                                     repeats: true)
    }
    
    @objc func timerDidFire() {
        time += 1
    }
    
    func resumeTimer(){
        startTimer()
    }
    
    func stopTimer(){
        timer?.invalidate()
        timer = nil
    }
    
    func resetTimer() {
        time = 0
    }
}
