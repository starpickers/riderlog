//
//  LocalNotificationManager.swift
//  riderLog
//
//  Created by 박서현 on 2021/08/03.
//

import Foundation
import UserNotifications

struct NotificationItem {
    var id: String
    var title: String
    var subtitle: String?
    var body: String
    var launchIn: Double
}

class LocalNotificationManager: NSObject, ObservableObject{
    
    var notifications = [NotificationItem]()
    
    override init() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, error in
            if granted == true && error == nil {
                print("Notifications permitted")
                
                let acceptAction = UNNotificationAction(identifier: "ACCEPT_ACTION",
                      title: "Accept",
                      options: UNNotificationActionOptions(rawValue: 0))
                let declineAction = UNNotificationAction(identifier: "DECLINE_ACTION",
                      title: "Decline",
                      options: UNNotificationActionOptions(rawValue: 0))
                
                let ecallActionCategory =
                      UNNotificationCategory(identifier: "ECALL_ACTION",
                      actions: [acceptAction, declineAction],
                      intentIdentifiers: [],
                      hiddenPreviewsBodyPlaceholder: "",
                      options: .customDismissAction)
                
                let center = UNUserNotificationCenter.current()
                center.setNotificationCategories([ecallActionCategory])
            } else {
                print("Notifications not permitted")
            }
        }
    }
    
    //MARK: Custom actions
    func registerCategories() {
        

    }
    
    func addNotification(title: String, subtitle: String?, body: String, launchIn: Double) -> Void {
        notifications.append(NotificationItem(id: UUID().uuidString, title: title, subtitle: subtitle, body: body, launchIn: launchIn))
    }
    
    func scheduleNotification() -> Void{
        for notification in notifications {
            
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { settings in
                guard (settings.authorizationStatus == .authorized) ||
                      (settings.authorizationStatus == .provisional) else { return }

//                if settings.alertSetting == .enabled {
//                    // Schedule an alert-only notification.
//                } else {
//                    // Schedule a notification with a badge and sound.
//                }
                
            }
            
            registerCategories()
            
            /*
             UI는 추후에 필요할 시 변경
             https://developer.apple.com/documentation/usernotificationsui/customizing_the_appearance_of_notifications
             */
            let content = UNMutableNotificationContent()
            content.title = notification.title
            if let subtitle = notification.subtitle {
                content.subtitle = subtitle
            }
            content.body = notification.body
            
            content.sound = UNNotificationSound.default
            
            content.categoryIdentifier = "ECALL_ACTION"
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: notification.launchIn, repeats: false)
            let request = UNNotificationRequest(identifier: notification.id, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { error in
                if let error = error {
                    print("local notification add request occurs error with \(error)")
                }
            }
        }
    }
}

extension LocalNotificationManager: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        let center  = UNUserNotificationCenter.current()
//            center.delegate = self
        
        switch response.actionIdentifier {
        case "ACCEPT_ACTION":
           break
             
        case "DECLINE_ACTION":
           break
             
        default:
           break
        }
        completionHandler()
    }
}
