//
//  Server.swift
//  riderLog
//
//  Created by 박서현 on 2021/09/29.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift
import FirebaseDatabase
import FirebaseStorage
import GeoFire

import Combine
import Network
import CoreLocation
import SwiftUI

extension Float {
   var bytes: [UInt8] {
       withUnsafeBytes(of: self, Array.init)
   }
}

extension Array {
    func pick(length: Int) -> [Element] {
        precondition(length >= 0, "lenth must not be negative")
        if length >= count { return self }
        let oldMax = Double(count - 1)
        let newMax = Double(length - 1)
        return (0..<length).map { self[Int((Double($0)*oldMax/newMax).rounded())]}
    }
}

extension CollectionReference {
    func whereField(_ field: String, isDateInToday value: Date) -> Query {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: value)
        
        guard
            let start = Calendar.current.date(from: components),
            let end = Calendar.current.date(byAdding: .day, value: 1, to: start)
        else {
            fatalError("Could not find start date or calculate end date.")
        }
        return whereField(field, isGreaterThan: start).whereField(field, isLessThan: end)
    }
    
    func whereField(_ field: String, isDateInMonth value: Date) -> Query {
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents([.year, .month], from: value)
        
        guard
            let start = calendar.date(from: components),
            let end = calendar.date(byAdding: DateComponents(month: 1, day: -1), to: start)
        else{
            fatalError("Could not find start date or calculate end date.")
        }
        return whereField(field, isGreaterThan: start).whereField(field, isLessThan: end)
    }
}

class Server {
    private let defaultFirestore = Firestore.firestore(app: FirebaseApp.app(name: "__FIRAPP_DEFAULT")!)
    private let controlFirestore = Firestore.firestore(app: FirebaseApp.app(name: "__FIRAPP_CONTROL")!)
    var ref: DatabaseReference = Database.database().reference()
    
    private let server_addr: NWEndpoint.Host = "www.riderlog.net"
    private let server_port: NWEndpoint.Port = 52810
    public var update_freq: Int = 100;
    
    var connection: NWConnection? = nil
    
    private var newDoc: DocumentReference? = nil
}

//MARK: Firebase POST Method
extension Server {
    ///Firestore - LIDERLOG-S
    func setRiding(from data: inout RiderLog) {
        let newDoc = defaultFirestore.collection("ridings").document()
        
        data.ridingDetail.ridingId = newDoc.documentID
        data.ridingDetail.startAt = Date()
        
        newDoc.setData([
            "createdAt": FieldValue.serverTimestamp(),
            "startAt": data.ridingDetail.startAt,
            "userId": data.userID!
        ])
    }
    
    func writeRealtimeDB(riderLog: RiderLog, IMU: [Int], ATT: [Float], GNSS: [Float], EVENTS: [Bool]) {
        let key = ref.child("logs").child(riderLog.ridingDetail.ridingId).childByAutoId().key ?? ""
        let data: [String: Any] = [
            "accel": [
                "x": IMU[0],
                "y": IMU[1],
                "z": IMU[2]
            ],
            "attitude": [
                "pitch": ATT[1],
                "roll": ATT[0]
            ],
            "createdAt": ServerValue.timestamp(),
            "events": [
                "collison": EVENTS[0],
                "overturn": EVENTS[1],
                "accelerate": EVENTS[2],
                "decelerate": EVENTS[3]
            ],
            "geopoints": [
                "lat": GNSS[0],
                "lon": GNSS[1]
            ],
            "gyro": [
                "x": IMU[0],
                "y": IMU[1],
                "z": IMU[2]
            ],
            "stat":[
                "bank":ATT[1],
                "speed": GNSS[2],
                "altitude": GNSS[3]
            ],
            "userId": riderLog.userID
        ]
        
        ref.child("logs").child(riderLog.ridingDetail.ridingId).child(key).setValue(data)
    }
    
    func finishRiding(from data: RidingDetail) {
        let postData: [String: Any] = [
            "bestAltitude": data.bestAltitude,
            "bestBank": data.bestBank,
            "bestSpeed": data.bestSpeed,
            "comment": data.comment,
            "finishAt": data.finishAt,
            "finishPlace": data.finishPlace,
            "mapImageUrl": data.mapImageURL,
            "photoUrls": data.photoURLs,
            "startPlace": data.startPlace,
            "totalDistance": Int(data.totalDistance),
            "totalTime": data.totaltime()
        ]
        
        /// 기존 데이터와 병합
        defaultFirestore.collection("ridings").document(data.ridingId).setData(postData, merge: true)
    }
    
    func updateStats(from ridingData: RidingDetail, userID: String) {
        let docRef = defaultFirestore.collection("stats").document(userID)
        docRef.getDocument { (document, err) in
            if let err = err {
                print(err.localizedDescription)
                return
            }
            
            if let document = document, document.exists {
                if let data = document.data() {
                    let overallDistance = data["overallDistance"] as? Int ?? 0
                    let overallTime = data["overallTime"] as? Int ?? 0
                    
                    docRef.setData([
                        "overallDistance": overallDistance + Int(ridingData.totalDistance),
                        "overallTime": overallTime + ridingData.totaltime()
                    ])
                }
                
                docRef.collection("histories")
                    .whereField("createdAt", isDateInToday: Date())
                    .getDocuments() { (documents, err) in
                        if let err = err {
                            print("Error getting documents: \(err)")
                            return
                        }

                        if let docs = documents?.documents {
                            if docs.count == 0 {
                                docRef.collection("histories").document().setData ([
                                    "bestAltitude": ridingData.bestAltitude,
                                    "bestBank": ridingData.bestBank,
                                    "bestSpeed": ridingData.bestSpeed,
                                    "createdAt": FieldValue.serverTimestamp(),
                                    "distance": Int(ridingData.totalDistance),
                                    "time": ridingData.totaltime()
                                ])
                            }
                            
                            for doc in docs {
                                let savedDistance = doc.data()["distance"] as! Int
                                let savedTime = doc.data()["time"] as! Int
                                let bestAltitude = doc.data()["bestAltitude"] as! Int
                                let bestBank = doc.data()["bestBank"] as! Int
                                let bestSpeed = doc.data()["bestSpeed"] as! Int
                                
                                docRef.collection("histories").document(doc.documentID).setData ([
                                    "bestAltitude": max(bestAltitude, ridingData.bestAltitude),
                                    "bestBank": max(bestBank, ridingData.bestBank),
                                    "bestSpeed": max(bestSpeed, ridingData.bestSpeed),
                                    "createdAt": FieldValue.serverTimestamp(),
                                    "distance": savedDistance + Int(ridingData.totalDistance),
                                    "time": savedTime + ridingData.totaltime()
                                ])
                            }
                        }
                    }
            } else {
                docRef.setData([
                    "overallDistance": Int(ridingData.totalDistance),
                    "overallTime": ridingData.totaltime(),
                ])
                docRef.collection("histories").document().setData([
                    "bestAltitude": ridingData.bestAltitude,
                    "bestBank": ridingData.bestBank,
                    "bestSpeed": ridingData.bestSpeed,
                    "createdAt": FieldValue.serverTimestamp(),
                    "distance": Int(ridingData.totalDistance),
                    "time": ridingData.totaltime()
                ])
            }
        }
    }
    
    func postEcall(userid: String, addr: String) {
        let ecallData: [String: Any] = [
            "createdAt": FieldValue.serverTimestamp(),
            "message": "[긴급] \(String(userid[..<userid.index(userid.startIndex, offsetBy: 8)]).uppercased()) 라이더 주행 중 사고를 감지하였습니다!\n\(addr)",
            "phoneNo": "01055600516",
            "status": "requested",
            "userId": userid
        ]
        
        defaultFirestore.collection("ecalls").addDocument(data: ecallData) { err in
            if let e = err {
                print("Error saving: \(e)")
            }
        }
    }
    
    
    //Firestore - RIDERLOG-MANAGEMENT-SYSTEM
    func writeDB(username: String, IMU: [Int], ATT: [Float], GNSS: [Float], EVENTS: [Bool]) {
        let hash = GFUtils.geoHash(forLocation: CLLocationCoordinate2D(latitude: Double(GNSS[0]), longitude: Double(GNSS[1])))
        let ridingData: [String: Any] = [
            "TIME": FieldValue.serverTimestamp(),
            "ACCEL": [IMU[0], IMU[1], IMU[2]],
            "ATTITUDE": [ATT[0], ATT[1], GNSS[0], GNSS[1]],
            "GYRO": [IMU[3], IMU[4], IMU[5]],
            "g": [
                "geohash": hash,
                "geopoint": GeoPoint(latitude: Double(GNSS[0]), longitude: Double(GNSS[1]))
            ],
            "EVENTS": [EVENTS[0], EVENTS[1], EVENTS[2], EVENTS[3]],
        ]
        
        controlFirestore.collection("driving").document(username).setData(ridingData) { (error) in
            if let e = error {
                print("Error saving: \(e)")
            }
        }
    }
}

//MARK: Firebase GET Method
extension Server {
    func getRidingsData(userid: String, completionHandler: @escaping ([FBRidings]) -> Void) {
        let ref = defaultFirestore.collection("ridings")
        var res: [FBRidings] = []
        
        ref.whereField("userId", isEqualTo: userid).order(by: "createdAt", descending: true).getDocuments { querySnapshot, error in
            DispatchQueue.global(qos: .default).async {
                if let error = error as NSError? {
                    print("Error getting document: \(error.localizedDescription)")
                } else {
                    for document in querySnapshot!.documents {
                        do {
                            if let data = try document.data(as: FBRiding.self) {
                                let result = FBRidings(id: document.documentID, data: data)
                                res.append(result)
                            }
                        } catch {
                            print(String(describing: error))
                        }
                    }
                }
                completionHandler(res)
            }
        }
    }
    
    func getOverallData(userid: String, completionHandler: @escaping (FBOverallData) -> Void) {
        defaultFirestore.collection("stats").document(userid).getDocument() { document, err in
            do {
                if let data = try document?.data(as: FBOverallData.self) {
                    completionHandler(data)
                }
            } catch {
                print(String(describing: error))
            }
        }
    }
    
    func getDailyRecord(userid: String, date: Date, completionHandler: @escaping (FBDailyRecord?) -> Void) {
        let ref = defaultFirestore.collection("stats").document(userid).collection("histories")
        
        ref.whereField("createdAt", isDateInToday: date).getDocuments() { querySnapshot, err in
            if let err = err as NSError? {
                print("Error getting document: \(err.localizedDescription)")
            } else {
                if querySnapshot!.documents.count == 0 {
                    completionHandler(nil)
                } else {
                    for doc in querySnapshot!.documents {
                        do {
                            if let data = try doc.data(as: FBDailyRecord.self) {
                                completionHandler(data)
                            }
                        } catch {
                            print(String(describing: error))
                        }
                    }
                }
            }
        }
    }
    
    func getMonthlyRecord(userid: String, date: Date, completionHandler: @escaping ([[Double]]) -> Void) {
        let ref = defaultFirestore.collection("stats").document(userid).collection("histories")
        var res: [[Double]] = [[Double](repeating:0, count: date.endOfMonth.day), [Double](repeating:0, count: date.endOfMonth.day), [Double](repeating:0, count: date.endOfMonth.day), [Double](repeating:0, count: date.endOfMonth.day), [Double](repeating:0, count: date.endOfMonth.day)]
        
        ref.whereField("createdAt", isDateInMonth: date).order(by: "createdAt").getDocuments() { querySnapshot, err in
            DispatchQueue.global(qos: .default).async {
                if let err = err as NSError? {
                    print("Error getting document: \(err.localizedDescription)")
                } else {
                    for doc in querySnapshot!.documents {
                        do {
                            if let data = try doc.data(as: FBDailyRecord.self) {
                                res[0][data.createdAt.day - 1] = Double(data.distance)/1000
                                res[1][data.createdAt.day - 1] = Double(data.time)/3600
                                res[2][data.createdAt.day - 1] = Double(data.bestSpeed)
                                res[3][data.createdAt.day - 1] = Double(data.bestAltitude)
                                res[4][data.createdAt.day - 1] = Double(data.bestBank)
                            }
                        } catch {
                            print(String(describing: error))
                        }
                    }
                }
                completionHandler(res)
            }
        }
    }
    
    func getRealtimeDB(riderLog: RiderLog, completionHandler: @escaping ([[String: Any]]) -> Void){
        var res: [[String: Any]] = []
        
        ref.child("logs").child(riderLog.ridingDetail.ridingId).queryOrdered(byChild: "createdAt").observeSingleEvent(of: .value) { snapshot in
            for child in snapshot.children {
                guard let snap = child as? DataSnapshot else { continue }
                if let dict = snap.value as? [String: Any] {
                    if let coord = dict["geopoints"] as? [String: Any], coord["lat"] as! Double != 0.0, coord["lon"] as! Double != 0.0{
                        res.append(coord)
                    }
                }
            }
            completionHandler(res)
        }
    }
    
    
    //MARK: Upload to Firebase Storage
    func uploadImage(image: UIImage, folder: String, name: String, completionHandler: @escaping (URL) -> Void) {
        let ref = Storage.storage(app: FirebaseApp.app(name: "__FIRAPP_DEFAULT")!).reference().child("images/" + folder + "/" + name + ".png")

        guard let data = image.pngData() else { return }
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"

        ref.putData(data, metadata: metaData) { (metadata, error) in
            if let error = error {
                print("error \(String(describing: error))")
                return
            }
            
            ref.downloadURL(completion: { (url, error) in
                guard let downloadURL = url else { return }
                completionHandler(downloadURL)
            })
        }
    }
}



//MARK: Local Server
extension Server {
    func writeFile(username: String, IMU: [Int], ATT: [Float], GNSS: [Float], EVENTS: [Bool]) {
        var data: [UInt8] = [UInt8](repeating:0, count: 50)
        data[0] = 0xFF;  // SoF
        data[1] = 0x30;  // MSG 길이
        
        // 유저 ID 변환
        let username: [UInt8] = [UInt8](username.utf8)
        data[2..<10] = username[0..<8]
        
        // IMU 데이터 배열 변환
        for i in 0..<6 {
            data[i*2+10] = UInt8(((IMU[i] >> 8) & 0xFF));
            data[i*2+11] = UInt8(IMU[i] & 0xFF);
        }
        
        // ATT 데이터 배열 변환
        for i in 0..<2 {
            data[i*4+22] = ATT[i].bytes[0]
            data[i*4+23] = ATT[i].bytes[1]
            data[i*4+24] = ATT[i].bytes[2]
            data[i*4+25] = ATT[i].bytes[3]
        }
        
        // GNSS 데이터 배열 변환
        for i in 0..<4 {
            data[i*4+30] = GNSS[i].bytes[0]
            data[i*4+31] = GNSS[i].bytes[1]
            data[i*4+32] = GNSS[i].bytes[2]
            data[i*4+33] = GNSS[i].bytes[3]
        }
        data[46] = EVENTS[0] ? UInt8(1) : UInt8(0)
        data[47] = EVENTS[1] ? UInt8(1) : UInt8(0)
        data[48] = EVENTS[2] ? UInt8(1) : UInt8(0)
        data[49] = EVENTS[3] ? UInt8(1) : UInt8(0)

        if self.connection == nil {
            self.connection = NWConnection(host: self.server_addr, port: self.server_port, using: .tcp)
            self.startReceive()
            self.connection?.stateUpdateHandler = { newState in
                switch newState {
                case .setup:
                    break
                case .waiting(let error):
                    print("is waiting: ", "\(error)")
                case .preparing:
                    break
                case .ready:
                    print("is ready")
                    break
                case .failed(let error):
                    print("did fail, error: ", "\(error)")
                    self.stop()
                case .cancelled:
                    print("was cancelled")
                    self.stop()
                @unknown default:
                    break
                }
            }
            self.connection?.start(queue: .global(qos: .utility))
        }
        self.send(data: data)
    }
    
    private func send(data: [UInt8]) {
        self.connection?.send(content: data, completion: NWConnection.SendCompletion.contentProcessed { error in
            if let error = error {
                print("did send, error: ", "\(error)")
                self.stop()
            }
        })
    }
    
    private func startReceive() {
        self.connection?.receive(minimumIncompleteLength: 1, maximumLength: 65536) { data, _, isDone, error in
            if let data = data, !data.isEmpty {
                print("did receive, data: ", data as NSData)
            }
            if let error = error {
                print("did receive, error: ", "\(error)")
                //데이터 끄면 에러 발생, 대응 필요
                self.stop()
                return
            }
            if isDone {
                print("did receive, EOF")
                self.stop()
                return
            }
            self.startReceive()
        }
    }
    
    func stop() {
        self.connection?.cancel()
        self.connection = nil
    }
}
