//
//  GeneralManager.swift
//  riderLog
//
//  Created by 박서현 on 2021/09/27.
//

import Foundation
import Combine
import CryptoKit
import CoreLocation
import CoreBluetooth
import UIKit

enum ECall {
    case request
    case detect
    case clear
}

struct RiderLog {
    enum RecordState {
        case START, PAUSE, STOP
    }
    var recordState: RecordState = .STOP
    var userID: String? = nil
    var ridingDetail: RidingDetail = RidingDetail()
}

struct RidingDetail: Codable {
    var ridingId: String = ""
    
    var startPlace: String = "출발지"
    var finishPlace: String = "도착지"
    var totalDistance: Float = 0
    var mapImageURL: String? = nil
    
    var startAt: Date? = nil
    var finishAt: Date? = nil
    
    var bestAltitude: Int = 0
    var bestBank: Int = 0
    var bestSpeed: Int = 0
    
    var photoURLs: [String]? = nil
    
    var comment: String? = nil
    
    func totaltime() -> Int{
        guard let startAt = startAt, let finishAt = finishAt else { return 0 }
        ///A TimeInterval value is always specified in seconds
        return Int(finishAt.timeIntervalSinceReferenceDate - startAt.timeIntervalSinceReferenceDate)
    }
}


class GeneralManager: ObservableObject {
    public var caliResult: Int = 0
    @Published var emergencyCall: ECall = .clear {
        didSet(oldValue) {
            if emergencyCall == .request {
                locationmanager.requestCurrentLocation()
            }
        }
    }
    
    @Published var sensor: Sensor = Sensor(isConnected: false, data: Sensor.Data())
    @Published var riderLog: RiderLog = RiderLog()
        
    private var getIMU: Bool = false
    private var workItemFB: DispatchWorkItem? = nil
    private var workItem: DispatchWorkItem? = nil
    
    private let ClientID = "tmza2gj1cp"
    private let ClientSecret = "4WrmQqtvfVrKnJkZs9C8wn6lmicfnrV5wljEngSw"
    
    @Published var blemanager = BLEManager()
    @Published var locationmanager = LocationManager()
    private var server = Server()
    
    @Published var timer = TimeCounter()
    
    var anyCancellable: AnyCancellable? = nil
    
    init() {
        anyCancellable = blemanager.objectWillChange.sink { [weak self] (_) in
            self?.objectWillChange.send()
        }
    }
    
    /*블루투스 및 GPS 등 권한 한꺼번에 관리하는 func 추가 필요*/
    
    //레코딩 관련 함수
    public func startREC() {
        riderLog.recordState = .START
        
        timer.startTimer()
        locationmanager.startUpdateLocation()
        //새로운 문서 생성
        self.server.setRiding(from: &riderLog)
        startService()
    }
    
    public func pauseREC() {
        riderLog.recordState = .PAUSE
        
        timer.stopTimer()
    }
    
    public func stopREC() {
        riderLog.recordState = .STOP
        
        timer.stopTimer()
        locationmanager.stopUpdataLocation()
        riderLog.ridingDetail.finishAt = Date()
        reverseGeocoding(lat: sensor.data.GPSDATA[0], lon: sensor.data.GPSDATA[1], type: "legalcode", result: .SHORT) { result in
            self.riderLog.ridingDetail.finishPlace = result
        }
        workItemFB?.cancel()
        workItemFB = nil
        workItem?.cancel()
        workItem = nil
    }

    public func saveREC(comnt: String, photos: [String] = []) {
        print(riderLog)
        riderLog.ridingDetail.comment = comnt
        riderLog.ridingDetail.photoURLs = photos
        DispatchQueue.global(qos: .background).async { [self] in
            while true {
                if riderLog.ridingDetail.finishPlace == "도착지" { continue }
                server.finishRiding(from: riderLog.ridingDetail)
                server.updateStats(from: riderLog.ridingDetail, userID: riderLog.userID!)
                resetData()
                break
            }
        }
    }
    
    public func saveUnsavedData(data: RidingDetail, comnt: String) {
        var ridingData: RidingDetail = data
        ridingData.comment = comnt
        server.finishRiding(from: ridingData)
        server.updateStats(from: ridingData, userID: riderLog.userID!)
        resetData()
    }
    
    func postEcall(addr: String) {
        server.postEcall(userid: riderLog.userID!, addr: addr)
    }
    
    func uploadImage(image: UIImage, folder: String, name: String = UUID().uuidString, completionHandler: @escaping (URL) -> Void) {
        server.uploadImage(image: image, folder: folder, name: name, completionHandler: { url in
            completionHandler(url)
        })
    }
    
    func resetData() {
        timer.resetTimer()
        riderLog.ridingDetail = RidingDetail()
        UserDefaults.standard.set(nil, forKey:"unsaved riding")
    }
    
    func getRidingsData(completionHandler: @escaping ([FBRidings]) -> Void){
        server.getRidingsData(userid: riderLog.userID!) { result in
            completionHandler(result)
        }
    }
    
    func getOverallData(completionHandler: @escaping (FBOverallData) -> Void) {
        server.getOverallData(userid: riderLog.userID!) { res in
            completionHandler(res)
        }
    }
    
    func getDailyRecord(date: Date, completionHandler: @escaping (FBDailyRecord?) -> Void) {
        server.getDailyRecord(userid: riderLog.userID!, date: date) { res in
            completionHandler(res)
        }
    }
    
    func getMonthlyRecord(date: Date, completionHandler: @escaping ([[Double]]) -> Void) {
        server.getMonthlyRecord(userid: riderLog.userID!, date: date) { res in
            completionHandler(res)
        }
    }
    
    func getRealtimeDB(completionHandler: @escaping ([[String: Any]]) -> Void) {
        server.getRealtimeDB(riderLog: riderLog) { response in
            completionHandler(response.pick(length: 100))
        }
    }
    
    //블루투스 연결 관련 함수
    func startScan() {
        blemanager.startScan()
    }
        
    func connect(to pheriphral: CBPeripheral) {
        blemanager.connect(to: pheriphral)
    }
    
    func disconnect() {
        blemanager.disconnect()
    }
    
    //데이터 요청 관련 함수
    func requestReboot() {
        blemanager.requestReboot();
    }

    func requestCalibrateAll(){
        blemanager.requestCalibrateAll();
    }

    func requestCalibrateAcc(){
        blemanager.requestCalibrateAcc();
    }

    func requestCalibrateGyro(){
        blemanager.requestCalibrateGyro();
    }

    func requestSetOrigin(){
        blemanager.requestSetOrigin();
    }

    func requestFirmUpdate(){
        blemanager.requestFirmUpdate();
    }

    func requestFactoryReset(){
        blemanager.requestFactoryReset();
    }
    
    func startService() {
        let userid = riderLog.userID!
        let rentalCompany = UserDefaults.standard.string(forKey: "RentalCompany") ?? "제주"

        workItemFB = DispatchWorkItem {
            while true {
                if self.workItemFB?.isCancelled ?? true { break }
                self.server.writeRealtimeDB(riderLog: self.riderLog, IMU: self.sensor.data.IMUDATA, ATT: self.sensor.data.ATTDATA, GNSS: self.sensor.data.GPSDATA, EVENTS: self.sensor.data.EVENTS)
                self.server.writeDB(username: rentalCompany[..<rentalCompany.index(rentalCompany.startIndex, offsetBy: 2)] + userid.uppercased(), IMU: self.sensor.data.IMUDATA, ATT: self.sensor.data.ATTDATA, GNSS: self.sensor.data.GPSDATA, EVENTS: self.sensor.data.EVENTS)
                Thread.sleep(forTimeInterval: 1)
            }
        }
        workItem = DispatchWorkItem {
            while true {
                if self.workItem?.isCancelled ?? true { break }
                if self.getIMU {
                    self.blemanager.requestIMU()
                    self.getIMU = false
                } else {
                    self.blemanager.requestAttitude()
                    self.getIMU = true
                }
                self.server.writeFile(username: String(userid[..<userid.index(userid.startIndex, offsetBy: 8)]).uppercased(), IMU: self.sensor.data.IMUDATA, ATT: self.sensor.data.ATTDATA, GNSS: self.sensor.data.GPSDATA, EVENTS: self.sensor.data.EVENTS)
                Thread.sleep(forTimeInterval: 0.01)
            }
        }
        DispatchQueue.global(qos: .default).async(execute: workItemFB!)
        DispatchQueue.global(qos: .default).async(execute: workItem!)
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.receive(_:)), name: Notification.Name("BLE_NOTIFY_DATA"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receive(_:)), name: Notification.Name("BLE_CONNECTION_STATUS"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.receive(_:)), name: Notification.Name("LOCATION_UPDATE"), object: nil)
    }
    
    @objc func receive(_ noti: Notification) {
        switch noti.name {
        case Notification.Name("BLE_CONNECTION_STATUS"):
            self.sensor.isConnected = noti.object as! Bool
            if !sensor.isConnected && riderLog.recordState != .STOP {
                blemanager.reconnect()
                riderLog.ridingDetail.finishAt = Date()
                reverseGeocoding(lat: sensor.data.GPSDATA[0], lon: sensor.data.GPSDATA[1], type: "legalcode", result: .SHORT) { result in
                    self.riderLog.ridingDetail.finishPlace = result
                    UserDefaults.standard.set(try? PropertyListEncoder().encode(self.riderLog.ridingDetail), forKey:"unsaved riding")
                }
            }
        case Notification.Name("BLE_NOTIFY_DATA"):
            if let dict = noti.object as? NSDictionary {
                switch dict["MSG_ID"] as? Int {
                case Sensor.SENSOR_MSG_CHECK_CONNECTION:
                    break
                case Sensor.SENSOR_MSG_REQUEST_FIRM_VERSION:
                    let firm_ver: [Int] = dict["MSG_PAYLOAD"] as! [Int]
                    sensor.data.FIRM_VER = "\(firm_ver[0]).\(firm_ver[1]).\(firm_ver[2])"
                case Sensor.SENSOR_MSG_IMU:
                    if riderLog.recordState == .START {
                        let imudata: [Int] = dict["MSG_PAYLOAD"] as! [Int]
                        for i in 0..<6 {
                            sensor.data.IMUDATA[i] = Int(Int32(bitPattern: UInt32((imudata[4*i] << 24) | (imudata[4*i+1] << 16) | (imudata[4*i+2] << 8) | imudata[4*i+3])));
                        }
                    }
                case Sensor.SENSOR_MSG_ATT:
                    if riderLog.recordState == .START {
                        let attdata: [Int] = dict["MSG_PAYLOAD"] as! [Int]
                        for i in 0..<2 {
                            sensor.data.ATTDATA[i] = Float(Double(Int32(bitPattern: UInt32((attdata[4*i] << 24) | (attdata[4*i+1] << 16) | (attdata[4*i+2] << 8) | attdata[4*i+3]))));
                        }
                        riderLog.ridingDetail.bestBank = max(riderLog.ridingDetail.bestBank, Int(sensor.data.ATTDATA[0]))
                    }
                case Sensor.ETC_MSG_EVENTS:
                    let data: [Int] = dict["MSG_PAYLOAD"] as! [Int]
                    for i in 0..<4 {
                        sensor.data.EVENTS[i] = Int(Int32(bitPattern: UInt32((data[4*i] << 24) | (data[4*i+1] << 16) | (data[4*i+2] << 8) | data[4*i+3]))) != 0
                    }
                case Sensor.SENSOR_MSG_REQUEST_ALL_CALI:
                    let all_cali: [Int] = dict["MSG_PAYLOAD"] as! [Int]
                    blemanager.requestCali = true
                    if all_cali[0] == 0x01 {
                        print("보정 완료")
                        caliResult = 1
                    } else if all_cali[1] == 0x02 {
                        print("보정 실패")
                        caliResult = 2
                    }
                case Sensor.SENSOR_MSG_REQUEST_GYRO_CALI:
                    let gryo_cali: [Int] = dict["MSG_PAYLOAD"] as! [Int]
                    blemanager.requestCali = true
                    if gryo_cali[0] == 0xFE {
                        print("보정 완료")
                        caliResult = 1
                    } else if gryo_cali[1] == 0xFF {
                        print("보정 실패")
                        caliResult = 2
                    }
                case Sensor.SENSOR_MSG_REQUEST_ACCEL_CALI:
                    let acc_cali: [Int] = dict["MSG_PAYLOAD"] as! [Int]
                    blemanager.requestCali = true
                    if acc_cali[0] == 0xFE {
                        print("보정 완료")
                        caliResult = 1
                    } else if acc_cali[1] == 0xFF {
                        print("보정 실패")
                        caliResult = 2
                    }
                case Sensor.SENSOR_MSG_REQUEST_ORIGIN_CALI:
                    let all_cali: [Int] = dict["MSG_PAYLOAD"] as! [Int]
                    blemanager.requestCali = true
                    if all_cali[0] == 0x01 {
                        print("보정 완료")
                        caliResult = 1
                    } else if all_cali[1] == 0x02 {
                        print("보정 실패")
                        caliResult = 2
                    }
                default:
                    break
                }
            }
        case Notification.Name("LOCATION_UPDATE"):
            let data = noti.object as! [Float]
            sensor.data.GPSDATA = data
            if riderLog.ridingDetail.startPlace == "출발지" {
                reverseGeocoding(lat: data[0], lon: data[1], type: "legalcode", result: .SHORT) { result in
                    self.riderLog.ridingDetail.startPlace = result
                }
            }
            riderLog.ridingDetail.bestSpeed = max(riderLog.ridingDetail.bestSpeed, Int(data[2])) as! Int
            riderLog.ridingDetail.bestAltitude = max(riderLog.ridingDetail.bestAltitude, Int(data[3])) as! Int
            riderLog.ridingDetail.totalDistance += data[4]
            if emergencyCall == .request {
                reverseGeocoding(lat: data[0], lon: data[1], type: "roadaddr", result: .FULL) { res in
                    self.postEcall(addr: res)
                }
            }
        default:
            break
        }
    }
}

enum AddressLength {
    case FULL
    case SHORT
}

extension GeneralManager {
    func reverseGeocoding(lat: Float, lon: Float, type: String, result: AddressLength, completionHandler: @escaping (String) -> Void) {
    
        var components = URLComponents(string: "https://naveropenapi.apigw.ntruss.com/map-reversegeocode/v2/gc")
        
        let requestname = URLQueryItem(name: "request", value: "coordsToaddr") //요청 이름
        let coords = URLQueryItem(name: "coords", value: "\(lon),\(lat)") //좌표
        let sourcecrs = URLQueryItem(name: "sourcecrs", value: "epsg:4326") //좌표계 코드
        let output = URLQueryItem(name: "output", value: "json")
        let orders = URLQueryItem(name: "orders", value: type) //변환 작업 이름, 법정동만 출력
        
        components?.queryItems = [requestname, coords, sourcecrs, orders, output]

        guard let url = components?.url else {
            print("Error: cannot create URL")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        //헤더
        request.setValue(ClientID, forHTTPHeaderField: "X-NCP-APIGW-API-KEY-ID")
        request.setValue(ClientSecret, forHTTPHeaderField: "X-NCP-APIGW-API-KEY")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error: error calling GET")
                print(error!)
                return
            }
            guard let data = data else {
                print("Error: Did not receive data")
                return
            }
            guard let response = response as? HTTPURLResponse, (200..<300) ~= response.statusCode else {
                let response = response as? HTTPURLResponse
                print("\(response!.statusCode) Error: HTTP request failed ")
                return
            }
            guard let output = try? JSONDecoder().decode(RGResponse.self, from: data) else {
                print("Error: JSON Data Parsing failed")
                return
            }
            
            if output.status.code == 0 {
                if result == .SHORT {
                    completionHandler("\(output.results[0].region.area1.name) \(output.results[0].region.area2.name)")
                } else if result == .FULL {
                    let fullAddress = "\(output.results[0].region.area1.name) \(output.results[0].region.area2.name) \(output.results[0].land!.name) \(output.results[0].land!.number1) \(output.results[0].addition0 != nil ? output.results[0].addition0!.value : "")"
                    completionHandler("\(fullAddress)")
                }
            } else {
                print("Error \(output.status.code): \(output.status.message)")
            }
        }.resume()
    }
}
