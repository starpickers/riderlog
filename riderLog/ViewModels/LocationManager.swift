//
//  LocationManager.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/13.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, ObservableObject {
    
    private var locationManager: CLLocationManager
    private var prevLocation: CLLocation? = nil
    
    public var data: [Float] = [Float](repeating: 0, count: 5)
    
    @Published var authorizationStatus: CLAuthorizationStatus
    
    var requestCustomAlert: Bool = false
    
    override init() {
        locationManager = CLLocationManager()
        authorizationStatus = locationManager.authorizationStatus
        
        super.init()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest //배터리로 동작할 경우 권장되는 가장 높은 수준의 정확도
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.showsBackgroundLocationIndicator = true
        locationManager.pausesLocationUpdatesAutomatically = false
        
        requestPermission()
        locationManager.delegate = self
    }
    
    func requestPermission() {
        if authorizationStatus == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        } else if authorizationStatus == .denied {
            requestCustomAlert = true
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if locationManager.authorizationStatus == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func startUpdateLocation() {
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdataLocation() {
        locationManager.stopUpdatingLocation()
    }
    
    func requestCurrentLocation() {
        locationManager.requestLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = locations.last else { return }
            
        data[0] = Float(location.coordinate.latitude)
        data[1] = Float(location.coordinate.longitude)
        data[2] = location.speed < 0 ? 0 : Float(location.speed * 3.6) //mps to kph
        data[3] = Float(location.altitude)
        
        if prevLocation != nil {
            data[4] = Float(location.distance(from: prevLocation!))
        }
        prevLocation = location

        print("Updated user's location: \(location)", "lat =", location.coordinate.latitude," long = ",location.coordinate.longitude)
        NotificationCenter.default.post(name: Notification.Name("LOCATION_UPDATE"), object: data, userInfo: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
        manager.stopUpdatingLocation()
    }
}
