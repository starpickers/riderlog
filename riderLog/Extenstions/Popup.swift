//
//  Popup.swift
//  riderLog
//
//  Created by 박서현 on 2021/12/02.
//

import SwiftUI

struct PopupWithItems<T: View, U: Identifiable>: ViewModifier {
    let popup: (U) -> T
    let item: Binding<U?>
    
    init(item: Binding<U?>, @ViewBuilder content: @escaping (U) -> T) {
        self.item = item
        popup = content
    }

    func body(content: Content) -> some View {
        content
            .overlay(popupContent())
    }

    @ViewBuilder private func popupContent() -> some View {
        if let item = item.wrappedValue {
            GeometryReader { geometry in
                popup(item)
                    .frame(width: geometry.size.width, height: geometry.size.height)
            }
        }
    }
}

struct PopupWithBool<T:View>: ViewModifier {
    let popup: () -> T
    let isPresented: Binding<Bool>
    
    init(isPresented: Binding<Bool>, @ViewBuilder content: @escaping () -> T) {
        self.isPresented = isPresented
        popup = content
    }
    
    func body(content: Content) -> some View {
        content
            .overlay(popupContent())
    }
    
    @ViewBuilder private func popupContent() -> some View {
        if isPresented.wrappedValue {
            GeometryReader { geometry in
                popup()
                    .frame(width: geometry.size.width, height: geometry.size.height)
            }
        }
    }
}

struct RoundedRectangleViewModifier: ViewModifier {
    let color: Color
    func body(content: Content) -> some View {
        content
            .padding()
            .background(color)
            .cornerRadius(8)
    }
}

extension View {
    func popup<T: View, U: Identifiable>(item: Binding<U?>, @ViewBuilder content: @escaping (U) -> T) -> some View {
        self.modifier(PopupWithItems(item: item, content: content))
    }
    
    func popup<T: View>(isPresented: Binding<Bool>, @ViewBuilder content: @escaping () -> T) -> some View {
        self.modifier(PopupWithBool(isPresented: isPresented, content: content))
    }
    
    func roundedRectangle(bgcolor color: Color) -> some View {
        modifier(RoundedRectangleViewModifier(color: color))
    }
}
