//
//  DateFormat.swift
//  riderLog
//
//  Created by 박서현 on 2021/11/01.
//

import Foundation

extension Date {
    public var year: Int {
        return Calendar.current.component(.year, from: self)
    }
    
    public var month: Int {
         return Calendar.current.component(.month, from: self)
    }
    
    public var day: Int {
         return Calendar.current.component(.day, from: self)
    }

    func toString(format: String = "MM-dd HH:mm") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension Date {
    public var startOfMonth: Date {
        let calendar = Calendar(identifier: .gregorian)
        var components = calendar.dateComponents([.year, .month, .hour], from: self)
        components.hour = 9
        
        return calendar.date(from: components)!
    }
    
    public var endOfMonth: Date {
        let calendar = Calendar(identifier: .gregorian)
        
        return calendar.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth)!
    }
}


extension Int {
    func toTimeFormat() -> String {
        let hours: Int = self / 3600
        let minutes: Int = (self % 3600) / 60
        let seconds: Int = (self % 3600) % 60
        
        return hours < 1 ? "\(minutes) : \(seconds.getStringFrom())" : "\(hours) : \(minutes) : \(seconds.getStringFrom())"
    }
    
    func getStringFrom() -> String {
        return self < 10 ? "0\(self)" : "\(self)"
    }
}
