//
//  HexColor.swift
//  riderLog
//
//  Created by 박서현 on 2021/07/13.
//

import SwiftUI

extension Color {
    
    public static var customGray : Color { Color(hex: 0xffffff, alpha: 0.5) }
    public static var darkGreen : Color { Color(hex: 0x219673) }
    
    init(hex: UInt, alpha: Double = 1) {
        self.init(
            .sRGB,
            red: Double((hex >> 16) & 0xff) / 255,
            green: Double((hex >> 08) & 0xff) / 255,
            blue: Double((hex >> 00) & 0xff) / 255,
            opacity: alpha
        )
    }
}
